require('dotenv').config();
const jwt = require('jsonwebtoken');
const db = require('../db.js');
const {User} = require('../classes/');

// users hardcoded for simplicity, store in a db for production applications
//const users = [{ id: 1, username: 'test', password: 'test', firstName: 'Test', lastName: 'User' }];

module.exports = {
    authenticate,
    getAll,
    verify,
    getUser_id
};

async function authenticate({ username, password }) {
    //const user = users.find(u => u.username === username && u.password === password);
    const user = await queryUser(username, password);

    if (!user) throw 'Username or password is incorrect';

    // create a jwt token that is valid for 7 days
    const token = jwt.sign({ user_id: user.user_id }, 'llamallamallama', { expiresIn: '7d' });

    return {
        ...omitPassword(user),
        token
    };
}

async function queryUser(username, password){
    const query = `SELECT * FROM users WHERE username = "${ username }" AND password = "${ password }"`;
    return await db.executeQuery(query).then(async result => {
        return new User(result[0].user_id, result[0].username, result[0].email, result[0].password, result[0].created_at);
    });
}

function getUser_id(req){
    let token = req.headers.authorization.split(' ')[1];
    const decoded = verify(token);
    return decoded.user_id;
}

function verify(token, secret = 'llamallamallama') {
    return jwt.verify(token, secret);
}

async function getAll() {
    return users.map(u => omitPassword(u));
}

// helper functions

function omitPassword(user) {
    const { password, ...userWithoutPassword } = user;
    return userWithoutPassword;
}