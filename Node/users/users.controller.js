const express = require('express');
const router = express.Router();
const userService = require('./user.service');
var bodyParser = require('body-parser')
var jsonParser = bodyParser.json()

// routes
router.post('/authenticate', jsonParser, async (req,  res, next) => {
    userService.authenticate(req.body)
        .then(user => res.json(user))
        .catch(next);
});


//router.get('/', getAll);

module.exports = router;


function getAll(req, res, next) {
    userService.getAll()
        .then(users => res.json(users))
        .catch(next);
}