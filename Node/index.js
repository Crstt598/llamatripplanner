require('dotenv').config();
const { Trip, Destination, Accommodation, Day, Activity, Expense, Note, User } = require('./classes');
const express = require('express');
const cors = require('cors');
const jwt = require('./helpers/jwt');
const errorHandler = require('./helpers/error-handler');
var bodyParser = require('body-parser')
var jsonParser = bodyParser.json()
const app = express();
const userService = require('./users/user.service');

app.use(cors());

app.use(jwt());
app.use('/users', require('./users/users.controller'));

// global error handler
app.use(errorHandler);

const db = require('./db');
const axios = require('axios');

app.get('/', (req, res) => {
  res.send('Hello from your Node.js backend!');
  exampleUsage();
});

function validateTripData(trip_name, start_date, end_date) {
  // Validate trip_name
  if (!trip_name || trip_name.trim() === '') {
  return { error: 'Trip name is required' };
  }

  // Validate start_date
  if (!start_date || isNaN(Date.parse(start_date))) {
  return { error: 'Invalid start date' };
  }

  // Validate end_date
  if (!end_date || isNaN(Date.parse(end_date))) {
  return { error: 'Invalid end date' };
  }

  return { success: true };
}

app.post('/save/trip', jsonParser, async (req,  res) => {
  try {

    req.body = escapeQuote(req.body);

    let trip_name = req.body.trip_name;
    let start_date = req.body.start_date;
    let end_date = req.body.end_date;

    const validation = validateTripData(trip_name, start_date, end_date);
    if (!validation.success) {
      return res.status(400).json({ message: validation.error });
    }

    let user = await db.getUser(userService.getUser_id(req));

    let trip = new Trip({
      user_id: user.user_id,
      trip_name: trip_name,
      start_date: new Date(start_date),
      end_date: new Date(end_date),
      location: req.body.location,
      description: req.body.description,
      destinations: [],
      accommodations: []
    });

    // Construct SQL query to insert data into the database
    let query = `INSERT INTO trips (user_id, trip_name, start_date, end_date, location, description) 
           VALUES (${trip.user_id}, '${trip.trip_name}', '${trip.start_date.toISOString().split('T')[0]}', '${trip.end_date.toISOString().split('T')[0]}', '${trip.location}', '${trip.description}')`;

    await db.executeQuery(query).then(async result => {
      console.log('Trip: ', result.insertId);
      trip.trip_id = result.insertId;
      res.status(200).json({ trip: trip });
    });

  } catch (err) {
    // Handle error
    //console.error(err);
    res.status(500).send('Internal Server Error');
  }
});

function validateDestinationData(destination_name, arrival_date, departure_date) {
  // Validate destination_name
  if (!destination_name || destination_name.trim() === '') {
    return { error: 'Destination name is required' };
  }

  // Validate arrival_date
  if (!arrival_date || isNaN(Date.parse(arrival_date))) {
    return { error: 'Invalid arrival date' };
  }

  // Validate departure_date
  if (!departure_date || isNaN(Date.parse(departure_date))) {
    return { error: 'Invalid departure date' };
  }

  return { success: true };
}

app.post('/save/destination', jsonParser, async (req,  res) => {
  try {
    let reDest = req.body.destination;
    let trip_id = req.body.trip_id;        

    const validation = validateDestinationData(reDest.destination_name, reDest.arrival_date, reDest.departure_date);
    if (!validation.success) {
      return res.status(400).json({ message: validation.error });
    }

    let destination = new Destination({
      destination_id: reDest.destination_id,
      destination_name: reDest.destination_name,
      arrival_date: new Date(reDest.arrival_date),
      departure_date: new Date(reDest.departure_date),
      description: reDest.description,
      days: []
    });

    // Construct SQL query to insert data into the database
    let query = `INSERT INTO destinations (trip_id, destination_name, arrival_date, departure_date, description)
                  VALUES (${trip_id}, '${destination.destination_name}', '${destination.arrival_date.toISOString().split('T')[0]}', '${destination.departure_date.toISOString().split('T')[0]}', '${destination.description}')`;

    await db.executeQuery(query).then(async result => {
      console.log('Destination: ', result.insertId);
      destination.destination_id = result.insertId;
      res.status(200).json({ destination: destination });
    });

  } catch (err) {
    // Handle error
    console.error(err);
    res.status(500).send('Internal Server Error');
  }
});

function validateDayData(date, day_number) {
  // Validate date
  if (!date || isNaN(Date.parse(date))) {
    return { error: 'Invalid date' };
  }

  // Validate day_number
  if (!day_number || isNaN(day_number)) {
    return { error: 'Invalid day number' };
  }

  return { success: true };
}

app.post('/save/day', jsonParser, async (req,  res) => {
  try {
    let reDay = req.body.day;
    let destination_id = req.body.destination_id;

    const validation = validateDayData(reDay.date, reDay.day_number);
    if (!validation.success) {
      return res.status(400).json({ message: validation.error });
    }

    const destinationExists = await db.executeQuery(`SELECT * FROM destinations WHERE destination_id = ${destination_id}`)
    
    if (destinationExists.length === 0) {
      return res.status(400).json({ message: 'Destination does not exist' });
    }
    

    let day = new Day({
      day_id: reDay.day_id,
      date: new Date(reDay.date),
      day_number: reDay.day_number,
      activities: []
    })

    // Construct SQL query to insert data into the database
    let query = `INSERT INTO days (destination_id, date, day_number) 
           VALUES (${destination_id}, '${day.date.toISOString().split('T')[0]}', ${day.day_number})`;

    await db.executeQuery(query).then(async result => {
      console.log('Day: ', result.insertId);
      day.day_id = result.insertId;
      res.status(200).json({ day: day });
    });

  } catch (err) {
    // Handle error
    console.error(err);
    res.status(500).send('Internal Server Error');
  }
});

app.post('/save/trip-whole', jsonParser, async (req,  res) => {
  try {

    let trip = new Trip({
      user_id: 1,
      trip_id: req.body.trip_id,
      trip_name: req.body.trip_name,
      start_date: new Date(req.body.start_date),
      end_date: new Date(req.body.end_date),
      location: "",
      description: "",
      destinations: [],
      accommodations: []
    });

    req.body.destinations.forEach(destination => {
      trip.destinations.push(new Destination({
        destination_id: destination.destination_id,
        destination_name: destination.destination_name,
        arrival_date: new Date(destination.arrival_date),
        departure_date: new Date(destination.departure_date),
        description: destination.description,
        days: []
      }));

      destination.days.forEach(day => {
        trip.destinations[trip.destinations.length - 1].days.push(new Day({
          day_id: day.day_id,
          date: new Date(day.date),
          day_number: day.day_number,
          activities: []
        }));
      });
    });

    // Construct SQL query to insert data into the database
    let query = `INSERT INTO trips (user_id, trip_name, start_date, end_date, location, description) 
           VALUES (${trip.user_id}, '${trip.trip_name}', '${trip.start_date.toISOString().split('T')[0]}', '${trip.end_date.toISOString().split('T')[0]}', '${trip.location}', '${trip.description}')`;

    await db.executeQuery(query).then(async result => {
      console.log('Trip: ', result.insertId);
      trip.trip_id = result.insertId;

      await saveDestinations(trip).then(() => {
        //console.log(trip);
        res.status(200).json({ trip: trip });
      });      
    });  
    
  } catch (err) {
    // Handle error
    console.error(err);
    res.status(500).send('Internal Server Error');
  }

  async function saveDestinations(trip) {
    const promises = trip.destinations.map(async (destination) => {
      let query = `INSERT INTO destinations (trip_id, destination_name, arrival_date, departure_date, description)
                  VALUES (${trip.trip_id}, '${destination.destination_name}', '${destination.arrival_date.toISOString().split('T')[0]}', '${destination.departure_date.toISOString().split('T')[0]}', '${destination.description}')`;
  
      const result = await db.executeQuery(query);
      //console.log('Destination: ', result.insertId);
      destination.destination_id = result.insertId;
  
      return saveDays(destination);
    });
  
    await Promise.all(promises);
  }
  
  async function saveDays(destination) {
    const promises = destination.days.map(async (day) => {
      let query = `INSERT INTO days (destination_id, date, day_number)
                  VALUES (${destination.destination_id}, '${day.date.toISOString().split('T')[0]}', ${day.day_number})`;
  
      const result = await db.executeQuery(query);
      //console.log('DAY: ', result.insertId);
      day.day_id = result.insertId;
    });
  
    await Promise.all(promises);
  }
});

function validateActivityData(activity_name, activity_time) {
  // Validate activity_name
  if (!activity_name || activity_name.trim() === '') {
    return { error: 'Activity name is required' };
  }

  // Validate activity_time
  if (!activity_time || !/^([01]\d|2[0-3]):([0-5]\d)$/.test(activity_time)) {
    return { error: 'Invalid activity time. Please use the format hh:mm' };
  }

  return { success: true };
}

app.post('/save/day-activities', jsonParser, async (req,  res) => {
  try {
    
    let day = new Day({
      day_id: req.body.day_id,
      date: new Date(req.body.date),
      day_number: req.body.day_number,
      activities: []
    })

    req.body.activities.forEach(async activity => {

      const validation = validateActivityData(activity.activity_name, activity.activity_time);
      if (!validation.success) {
        return res.status(400).json({ message: validation.error });
      }

      day.activities.push(new Activity({
        activity_id: activity.activity_id,
        activity_name: activity.activity_name,
        activity_url: activity.url,
        activity_note: activity.activity_note,
        activity_time: activity.time,
        activity_eta: activity.activity_eta
      }));
      
      const promises = day.activities.map(async (activity) => {
        let query = `INSERT INTO activities (day_id, activity_name, activity_url, activity_note, activity_time, activity_eta)
                    VALUES (${day.day_id}, '${activity.activity_name}', '${activity.activity_url}', '${activity.activity_note}', '${activity.activity_time}', '${activity.activity_eta}')`;
    
        const result = await db.executeQuery(query);
        console.log('Activity: ', result.insertId);
        activity.activity_id = result.insertId;
      });
    
      await Promise.all(promises);
    });

  } catch (err) {
    // Handle error
    console.error(err);
    res.status(500).send('Internal Server Error');
  }
  
});

app.post('/save/activity', jsonParser, async (req,  res) => {
  try {
    let day_id = req.body.day_id;
    let act = req.body.activity;

    act = escapeQuote(act);

    let activity = new Activity({
      activity_name: act.activity_name || act.activity,
      activity_url: act.uractivity_url || '',
      activity_note: act.activity_note || act.description || '', 
      activity_time: act.activity_time || act.time || 0,
      activity_eta: act.activity_eta || 0
    });

    const validation = validateActivityData(activity.activity_name, activity.activity_time);
    if (!validation.success) {
      return res.status(400).json({ message: validation.error });
    }

    // Verify if day_id exists
    const dayExists = await db.executeQuery(`SELECT * FROM days WHERE day_id = ${day_id}`);
    if (dayExists.length === 0) {
      return res.status(400).json({ message: 'Day does not exist' });
    }

    // Construct SQL query to insert data into the database
    let query = `INSERT INTO activities (day_id, activity_name, activity_url, activity_note, activity_time, activity_eta)
           VALUES (${day_id}, '${activity.activity_name}', '${activity.activity_url}', '${activity.activity_note}', '${activity.activity_time}', '${activity.activity_eta}')`;

    await db.executeQuery(query).then(async result => {
      console.log('Activity: ', result.insertId);
      activity.activity_id = result.insertId;
      res.status(200).json({day_id: day_id, activity: activity });
    });

  } catch (err) {
    // Handle error
    console.error(err);
    res.status(500).send('Internal Server Error');
  }
});

app.post('/update/activity', jsonParser, async (req,  res) => {
  try {
    let act = req.body.activity;

    const validation = validateActivityData(act.activity_name, act.activity_time);
    if (!act.activity_id) {
      return res.status(400).json({ message: 'Activity ID is required' });
    }
    if (!validation.success) {
      return res.status(400).json({ message: validation.error });
    }

    // Verify if activity_id exists
    const activityExists = await db.executeQuery(`SELECT * FROM activities WHERE activity_id = ${act.activity_id}`);
    if (activityExists.length === 0) {
      return res.status(400).json({ message: 'Activity does not exist' });
    }

    let activity = new Activity({
      activity_id: act.activity_id,
      activity_name: act.activity_name,
      activity_url: act.activity_url,
      activity_note: act.activity_note.replaceAll(/'/g, "''"), //Todo: Fix this. It does not escape single quotes
      activity_time: act.activity_time,
      activity_eta: act.activity_eta
    });

    // Construct SQL query to insert data into the database
    let query = `UPDATE activities SET activity_name = '${activity.activity_name}', activity_note = '${activity.activity_note}', activity_time = '${activity.activity_time}', activity_eta = '${activity.activity_eta}' WHERE activity_id = ${activity.activity_id}`;

    await db.executeQuery(query).then(async result => {
      console.log('Udated Activity: ', result.insertId);
      res.status(200).json({ activity: activity });
    });

  } catch (err) {
    // Handle error
    console.error(err);
    res.status(500).send('Internal Server Error');
  }
});

app.post('/delete/activity', jsonParser, async (req,  res) => {
  try {
    let activity_id = req.body.activity_id;
    
    let query = `DELETE FROM activities WHERE activity_id = ${activity_id}`;

    await db.executeQuery(query).then(async result => {
      console.log('Deleted Activity: ', activity_id);
      res.status(200).json({'Deleted Activity ': activity_id});
    });

  } catch (err) {
    // Handle error
    console.error(err);
    res.status(500).send('Internal Server Error');
  }
});

app.get('/get/trip_ids', async (req,  res) => {
  try {
    let query = `SELECT trip_id, trip_name FROM trips WHERE user_id = 1`;

    await db.executeQuery(query).then(async result => {
      res.status(200).json({trips: result});
    });

  } catch (err) {
    // Handle error
    console.error(err);
    res.status(500).send('Internal Server Error');
  }
});


function escapeQuote(obj) {
  Object.keys(obj).forEach(key => {
    if (typeof obj[key] === 'string') {
      obj[key] = obj[key].replaceAll(/'/g, "''");
    }
  });
  return obj;
}

// Function to get trip data
async function getTripData(trip_id, user) {
  let tripQuery = `SELECT * FROM trips WHERE trip_id = ${trip_id}`;
  let tripResults = await db.executeQuery(tripQuery);

  if (tripResults.length === 0) {
    return null;
  }

  let tripData = tripResults[0];

  if (user.user_id !== tripData.user_id) {
    return 'Forbidden';
  }

  return {
    tripData: tripData,
    destinations: await getDestinations(trip_id)
  };
}

// Function to get destinations for a trip
async function getDestinations(trip_id) {
  let destinationQuery = `SELECT * FROM destinations WHERE trip_id = ${trip_id}`;
  let destinationResults = await db.executeQuery(destinationQuery);

  let destinations = [];

  for (const destinationData of destinationResults) {
    destinations.push({
      destinationData: destinationData,
      days: await getDays(destinationData.destination_id)
    });
  }

  return destinations;
}

// Function to get days for a destination
async function getDays(destination_id) {
  let dayQuery = `SELECT * FROM days WHERE destination_id = ${destination_id}`;
  let dayResults = await db.executeQuery(dayQuery);

  let days = [];

  for (const dayData of dayResults) {
    days.push({
      dayData: dayData,
      activities: await getActivities(dayData.day_id)
    });
  }

  return days;
}

// Function to get activities for a day
async function getActivities(day_id) {
  let activityQuery = `SELECT * FROM activities WHERE day_id = ${day_id}`;
  let activityResults = await db.executeQuery(activityQuery);

  let activities = [];

  for (const activityData of activityResults) {
    activities.push(new Activity({
      activity_id: activityData.activity_id,
      activity_name: activityData.activity_name,
      activity_url: activityData.activity_url,
      activity_note: activityData.activity_note,
      activity_time: activityData.activity_time,
      activity_eta: activityData.activity_eta
    }));
  }

  return activities;
}

app.get('/get/trip/', async (req, res) => {
  try {
    let trip_id = req.query.trip_id;
    let user = await db.getUser(userService.getUser_id(req));

    let tripData = await getTripData(trip_id, user);

    if (!tripData) {
      res.status(404).send('Trip not found');
      return;
    }

    if (tripData === 'Forbidden') {
      res.status(403).send('Forbidden');
      return;
    }

    let trip = new Trip({
      user_id: tripData.tripData.user_id,
      trip_id: tripData.tripData.trip_id,
      trip_name: tripData.tripData.trip_name,
      start_date: new Date(tripData.tripData.start_date),
      end_date: new Date(tripData.tripData.end_date),
      location: tripData.tripData.location,
      description: tripData.tripData.description,
      destinations: tripData.destinations.map(destination => ({
        destination_id: destination.destinationData.destination_id,
        destination_name: destination.destinationData.destination_name,
        arrival_date: new Date(destination.destinationData.arrival_date),
        departure_date: new Date(destination.destinationData.departure_date),
        description: destination.destinationData.description,
        days: destination.days.map(day => ({
          day_id: day.dayData.day_id,
          date: new Date(day.dayData.date),
          day_number: day.dayData.day_number,
          activities: day.activities
        }))
      }))
    });

    res.status(200).json({ trip: trip });

  } catch (err) {
    // Handle error
    console.error(err);
    res.status(500).send('Internal Server Error');
  }
});

app.get('/get/all-trips', async (req,  res) => {
  try {
    let user = await db.getUser(userService.getUser_id(req));
    //console.log(user);

    let query = `SELECT * FROM trips WHERE user_id = ${user.user_id}`;

    await db.executeQuery(query).then(async result => {
      res.status(200).json(result);
    });

  } catch (err) {
    // Handle error
    console.error(err);
    res.status(500).send('Internal Server Error');
  }
});

app.post('/v1/chat/completions', jsonParser, async (req, res) => {
  try {
    const requestData = req.body;

    axios.post(process.env.LLAMAAPIURL, requestData)
      .then(response => {
        res.status(200).json(response.data);
      })
      .catch(error => {
        console.error(error);
        res.status(500).send('Internal Server Error');
      });

  } catch (err) {
    // Handle error
    console.error(err);
    res.status(500).send('Internal Server Error');
  }
});


// Specify port to listen on
const port = process.env.PORT || 3000; // Use environment variable or default to 3000

var server = app.listen(port, () => {
  //console.log(`Server listening on port ${port}`);
});

app.close = (() => {
  //console.log('Closing server');
  server.close();
});

module.exports = {
  app,
  getTripData,
  getDestinations,
  getDays,
  getActivities
};