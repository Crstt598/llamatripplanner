  // Expense.js
  class Expense {
    constructor(init = {}) {
      this.expense_id = init.expense_id || -1;
      this.expense_name = init.expense_name || '';
      this.amount = init.amount || 0;
      this.category = init.category;
      this.description = init.description;
      this.expense_date = init.expense_date;
    }
  }

module.exports = Expense;