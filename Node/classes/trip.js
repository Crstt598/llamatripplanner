// Trip.js
class Trip {
  constructor(init = {}) {
    this.trip_id = init.trip_id || -1;
    this.user_id = init.user_id;
    this.trip_name = init.trip_name || '';
    this.start_date = init.start_date;
    this.end_date = init.end_date;
    this.location = init.location;
    this.description = init.description;
    this.destinations = init.destinations || [];
    this.accommodations = init.accommodations || [];
  }
}

module.exports = Trip;
  