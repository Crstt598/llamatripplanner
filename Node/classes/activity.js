  // Activity.js
  class Activity {
    constructor(init = {}) {
      this.activity_id = init.activity_id || -1;
      this.activity_name = init.activity_name || '';
      this.activity_url = init.activity_url;
      this.activity_note = init.activity_note;
      this.activity_time = init.activity_time;
      this.activity_eta = init.activity_eta;
    }
  }

module.exports = Activity;