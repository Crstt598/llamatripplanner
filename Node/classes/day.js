
// Day.js
class Day {
constructor(init = {}) {
    this.day_id = init.day_id || 0;
    this.date = init.date;
    this.destination = init.destination;
    this.day_number = init.day_number;
    this.description = init.description;
    this.activities = init.activities || [];
    this.expenses = init.expenses || [];
    this.notes = init.notes || [];
}
}

module.exports = Day