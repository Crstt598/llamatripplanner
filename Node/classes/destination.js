 // Destination.js
 class Destination {
  constructor(init = {}) {
    this.destination_id = init.destination_id || -1;
    this.destination_name = init.destination_name || '';
    this.arrival_date = init.arrival_date;
    this.departure_date = init.departure_date;
    this.description = init.description;
    this.days = init.days || [];
  }
}

module.exports = Destination;