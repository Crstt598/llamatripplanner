//export * from './trip';
//export * from './user';

const Trip = require("./trip.js");
const Destination = require("./destination.js");
const Day = require("./day.js");
const Activity = require("./activity.js");
const Accommodation = require("./accommodation.js");
const Expense = require("./expense.js");
const Note = require("./note.js");
const User = require("./user.js");

module.exports = {
    Trip,
    Destination,
    Accommodation,
    Day,
    Activity,
    Expense,
    Note,
    User
  };
