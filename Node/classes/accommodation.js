  // Accommodation.js
  class Accommodation {
    constructor(init = {}) {
      this.accommodation_id = init.accommodation_id || -1;
      this.accommodation_name = init.accommodation_name || '';
      this.check_in_date = init.check_in_date;
      this.check_out_date = init.check_out_date;
      this.address = init.address;
      this.contact_number = init.contact_number;
      this.description = init.description;
      this.accommodation_url = init.accommodation_url;
    }
  }

  module.exports = Accommodation;