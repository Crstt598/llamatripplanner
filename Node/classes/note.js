  // Note.js
class Note {
    constructor(init = {}) {
      this.note_id = init.note_id || -1;
      this.note_title = init.note_title;
      this.note_content = init.note_content;
      this.created_at = init.created_at;
    }
  }

module.exports = Note;