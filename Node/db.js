const mysql = require('mysql2/promise');
const {User} = require('./classes/');


const pool = mysql.createPool({
  host: process.env.DB_HOST || 'localhost', // Use environment variable or default
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,  
  waitForConnections: true,
  connectionLimit: 99,
});

let connection = null;

// Initialization function
async function init() {
  try {
    connection = null;
    connection = await pool.getConnection();
    console.log('Connected to MySQL database!');
  } catch (err) {
    console.error(err);
  }
}
  
// Call the initialization function
init();

async function executeQuery(query) {
  //console.log('Executing query:', query);
  try {
    if (!connection || connection.state === 'disconnected') {
      await init();
    }
    //const connection = await pool.getConnection();
    const [rows, fields] = await connection.query(query);
    //connection.release();
    return rows;
  } catch (err) {
    console.error(err);
    //throw err; // Re-throw the error for the caller to handle if needed
  }
}

async function getUser(user_id){

  const query = 'SELECT * FROM users WHERE user_id = ' + user_id;
  return await executeQuery(query).then(async result => {
    return new User(result[0].user_id, result[0].username, result[0].email, result[0].password, result[0].created_at);
  });
}

function closeConnection() {
  connection.end();
}

module.exports = {
  executeQuery,
  getUser,
  closeConnection
};