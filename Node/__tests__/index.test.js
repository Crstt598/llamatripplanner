const { Trip, Destination, Accommodation, Day, Activity, Expense, Note, User } = require('../classes');
const db = require('../db');
const request = require('supertest');
const { app, getTripData, getDestinations, getDays } = require('../index');
const { json } = require('body-parser');

const mockToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxLCJpYXQiOjE3MTM3OTc4MTYsImV4cCI6MTcxNDQwMjYxNn0.UngL4evfCsBBn9jR28oAoNmRy4Zxtvuzu4CkNbjnLXA';

// Mock the database connection
jest.mock('../db', () => ({
  executeQuery: jest.fn(),
  getUser: jest.fn(),
  closeConnection: jest.fn(),
}));


describe('getTripData', () => {
  it('should return null if no trip is found', async () => {
    // Mock the database query result
    db.executeQuery.mockResolvedValueOnce([]);
    const user = new User(999, 'testuser', 'test@example.com', 'password', new Date());
    db.getUser.mockResolvedValueOnce(user);

    const tripData = await getTripData(1, user);
    expect(tripData).toBeNull();
  });

  // Add more test cases for getTripData
});

describe('getDestinations', () => {
  it('should return an empty array if no destinations are found', async () => {
    // Mock the database query result
    db.executeQuery.mockResolvedValueOnce([]);

    const destinations = await getDestinations(1);
    expect(destinations).toEqual([]);
  });

  // Add more test cases for getDestinations
});

describe('GET /get/all-trips', () => {
    
    it('should return all trips for the authenticated user', async () => {
        // Mock the database query result
        const mockTrips = [
            { trip_id: 1, trip_name: 'Trip 1', /* other trip properties */ },
            { trip_id: 2, trip_name: 'Trip 2', /* other trip properties */ },
        ];
        db.executeQuery.mockResolvedValueOnce(mockTrips);
    
        // Mock the authenticated user
        const mockUser = { user_id: 1 };
        db.getUser.mockResolvedValueOnce(mockUser);

        const response = await request(app).get('/get/all-trips').set('Authorization', `Bearer ${mockToken}`).then(response => {
    
            expect(response.status).toBe(200);
            expect(response.body).toEqual(mockTrips);
        });
    });
  
    // Add more test cases for error scenarios, authentication, etc.
});

describe('GET /get/trip', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should return the trip data for the given trip_id and authenticated user', async () => {
    
    const mocktripString = `{ 
      "trip": {
        "trip_id": 3,
        "user_id": 1,
        "trip_name": "Korean Adventure",
        "start_date": "2024-03-01T00:00:00.000Z",
        "end_date": "2024-03-07T00:00:00.000Z",
        "location": "Korea",
        "description": "A Korean Adventure taking place in March 2024.",
        "destinations": [
          {
            "destination_id": 6,
            "destination_name": "Seoul",
            "arrival_date": "2024-03-01T00:00:00.000Z",
            "departure_date": "2024-03-05T00:00:00.000Z",
            "days": [
              {
                "day_id": 11,
                "date": "2024-03-01T00:00:00.000Z",
                "day_number": 1,
                "activities": [
                  {
                    "activity_id": 30,
                    "activity_name": "Gyeongbokgung Palace Tour",
                    "activity_url": "",
                    "activity_note": "Explore the largest and most iconic palace in Seoul",
                    "activity_time": "09:00:00",
                    "activity_eta": "00:00:00"
                  }
                ]
              }
            ]
          }
        ],
        "accommodations": []
      }
    }`;

    // Mock the database query results
    const mockTripResult = [
        {
          trip_id: 3,
          user_id: 1,
          trip_name: 'Korean Adventure',
          start_date: '2024-03-01',
          end_date: '2024-03-07',
          location: 'Korea',
          description: 'A Korean Adventure taking place in March 2024.',
        }
      ];
  
      const mockDestinationResults = [
        {
          destination_id: 6,
          destination_name: 'Seoul',
          arrival_date: '2024-03-01',
          departure_date: '2024-03-05',
          description: undefined,
        }
      ];
  
      const mockDayResults = [
        {
          day_id: 11,
          date: '2024-03-01',
          day_number: 1,
        }
      ];
  
      const mockActivityResults = [
        {
          activity_id: 30,
          activity_name: 'Gyeongbokgung Palace Tour',
          activity_url: '',
          activity_note: 'Explore the largest and most iconic palace in Seoul',
          activity_time: '09:00:00',
          activity_eta: '00:00:00',
        }
      ];
  
      db.executeQuery
        .mockResolvedValueOnce(mockTripResult)
        .mockResolvedValueOnce(mockDestinationResults)
        .mockResolvedValueOnce(mockDayResults)
        .mockResolvedValueOnce(mockActivityResults);

    // Mock the authenticated user
    const mockUser = { user_id: 1 };
    db.getUser.mockResolvedValueOnce(mockUser);

    const response = await request(app)
      .get('/get/trip/')
      .query({ trip_id: 3 })
      .set('Authorization', `Bearer ${mockToken}`);

    let jsonTrip = JSON.parse(mocktripString);

    expect(response.status).toBe(200);
    expect(response.body).toEqual(jsonTrip);

  });

  it('should return 404 if the trip is not found', async () => {
    // Mock the database query result
    db.executeQuery.mockResolvedValueOnce([]);

    // Mock the authenticated user
    const mockUser = { user_id: 1 };
    db.getUser.mockResolvedValueOnce(mockUser);

    const response = await request(app)
      .get('/get/trip/')
      .query({ trip_id: 999 })
      .set('Authorization', `Bearer ${mockToken}`);

    expect(response.status).toBe(404);
    expect(response.text).toBe('Trip not found');
  });

  it('should return 403 if the user is not authorized to access the trip', async () => {
    // Mock the database query result
    const mockTripData = [{
        user_id: 2, // Different user_id
    }];
    db.executeQuery.mockResolvedValueOnce(mockTripData);

    // Mock the authenticated user
    const mockUser = { user_id: 1 };
    db.getUser.mockResolvedValueOnce(mockUser);

    const response = await request(app)
      .get('/get/trip/')
      .query({ trip_id: 3 })
      .set('Authorization', `Bearer ${mockToken}`);

    expect(response.status).toBe(403);
    expect(response.text).toBe('Forbidden');
  });
});

describe('POST /save/trip', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should save a new trip and return the trip object', async () => {
    // Mock the database query result
    const mockInsertResult = { insertId: 1 };
    db.executeQuery.mockResolvedValueOnce(mockInsertResult);

    const tripData = {
      trip_name: 'Test Trip',
      start_date: '2023-06-01',
      end_date: '2023-06-07',
      location: 'Test Location',
      description: 'Test Description',
    };

    const response = await request(app)
      .post('/save/trip')
      .send(tripData)
      .set('Authorization', `Bearer ${mockToken}`);

    const expectedTrip = new Trip({
      user_id: 1,
      trip_id: mockInsertResult.insertId,
      trip_name: tripData.trip_name,
      start_date: new Date(tripData.start_date),
      end_date: new Date(tripData.end_date),
      location: tripData.location,
      description: tripData.description,
      destinations: [],
      accommodations: [],
    });

    let jsonTrip = JSON.parse(JSON.stringify(expectedTrip));

    expect(response.status).toBe(200);
    expect(response.body).toEqual({ trip: jsonTrip });
  });

  it('should handle invalid data and return a 400 status code', async () => {
    // Mock an invalid data scenario
    const invalidTripData = {
      trip_name: '', // Empty trip name is invalid
      start_date: '2023-06-01',
      end_date: '2023-06-07',
      location: 'Test Location',
      description: 'Test Description',
    };

    const response = await request(app)
      .post('/save/trip')
      .send(invalidTripData)
      .set('Authorization', `Bearer ${mockToken}`);

    expect(response.status).toBe(400);
    // Add additional assertions based on your error handling implementation
  });

  // Add more test cases for different scenarios, such as handling special characters, large input data, or boundary conditions
});

describe('POST /save/destination', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should return 400 if destination name is missing', async () => {
    const destinationData = {
      trip_id: 1,
      destination: {
        destination_name: '',
        arrival_date: '2023-06-01',
        departure_date: '2023-06-07',
        description: 'Test Description',
      },
    };

    const response = await request(app)
      .post('/save/destination')
      .send(destinationData)
      .set('Authorization', `Bearer ${mockToken}`);

    expect(response.status).toBe(400);
    expect(response.body).toEqual({ message: 'Destination name is required' });
  });

  it('should return 400 if arrival date is invalid', async () => {
    const destinationData = {
      trip_id: 1,
      destination: {
        destination_name: 'Test Destination',
        arrival_date: 'invalid-date',
        departure_date: '2023-06-07',
        description: 'Test Description',
      },
    };

    const response = await request(app)
      .post('/save/destination')
      .send(destinationData)
      .set('Authorization', `Bearer ${mockToken}`);

    expect(response.status).toBe(400);
    expect(response.body).toEqual({ message: 'Invalid arrival date' });
  });

  it('should return 400 if departure date is invalid', async () => {
    const destinationData = {
      trip_id: 1,
      destination: {
        destination_name: 'Test Destination',
        arrival_date: '2023-06-01',
        departure_date: 'invalid-date',
        description: 'Test Description',
      },
    };

    const response = await request(app)
      .post('/save/destination')
      .send(destinationData)
      .set('Authorization', `Bearer ${mockToken}`);

    expect(response.status).toBe(400);
    expect(response.body).toEqual({ message: 'Invalid departure date' });
  });

  it('should save a new destination and return the destination object', async () => {
    // Mock the database query result
    const mockInsertResult = { insertId: 1 };
    db.executeQuery.mockResolvedValueOnce(mockInsertResult);

    const destinationData = {
      trip_id: 1,
      destination: {
        destination_name: 'Test Destination',
        arrival_date: '2023-06-01',
        departure_date: '2023-06-07',
        description: 'Test Description',
      },
    };

    const response = await request(app)
      .post('/save/destination')
      .send(destinationData)
      .set('Authorization', `Bearer ${mockToken}`);

    const expectedDestination = new Destination({
      destination_id: mockInsertResult.insertId,
      destination_name: destinationData.destination.destination_name,
      arrival_date: new Date(destinationData.destination.arrival_date),
      departure_date: new Date(destinationData.destination.departure_date),
      description: destinationData.destination.description,
      days: [],
    });

    jsonDestination = JSON.parse(JSON.stringify(expectedDestination));

    expect(response.status).toBe(200);
    expect(response.body).toEqual({ destination: jsonDestination });
  });  
});

describe('POST /save/day', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should return 400 if date is missing', async () => {
    const dayData = {
      destination_id: 1,
      day: {
        date: '',
        day_number: 1,
      },
    };

    const response = await request(app)
      .post('/save/day')
      .send(dayData)
      .set('Authorization', `Bearer ${mockToken}`);

    expect(response.status).toBe(400);
    expect(response.body).toEqual({ message: 'Invalid date' });
  });

  it('should return 400 if day number is missing', async () => {
    const dayData = {
      destination_id: 1,
      day: {
        date: '2023-06-01',
        day_number: '',
      },
    };

    const response = await request(app)
      .post('/save/day')
      .send(dayData)
      .set('Authorization', `Bearer ${mockToken}`);

    expect(response.status).toBe(400);
    expect(response.body).toEqual({ message: 'Invalid day number' });
  });

  it('should return 400 if date is invalid', async () => {
    const dayData = {
      destination_id: 1,
      day: {
        date: 'invalid-date',
        day_number: 1,
      },
    };

    const response = await request(app)
      .post('/save/day')
      .send(dayData)
      .set('Authorization', `Bearer ${mockToken}`);

    expect(response.status).toBe(400);
    expect(response.body).toEqual({ message: 'Invalid date' });
  });

  it('should return 400 if day number is invalid', async () => {
    const dayData = {
      destination_id: 1,
      day: {
        date: '2023-06-01',
        day_number: 'invalid-number',
      },
    };

    const response = await request(app)
      .post('/save/day')
      .send(dayData)
      .set('Authorization', `Bearer ${mockToken}`);

    expect(response.status).toBe(400);
    expect(response.body).toEqual({ message: 'Invalid day number' });
  });

  it('should return 400 if the destination is not found', async () => {
    // Mock the database query result
    db.executeQuery.mockResolvedValueOnce([]);

    const dayData = {
      destination_id: 999, // Invalid destination_id
      day: {
        date: '2023-06-01',
        day_number: 1,
      },
    };

    const response = await request(app)
      .post('/save/day')
      .send(dayData)
      .set('Authorization', `Bearer ${mockToken}`);

    expect(response.status).toBe(400);
    expect(response.body).toEqual({ message: 'Destination does not exist'});
  });

  it('should save a new day and return the day object', async () => {
    // Mock the database query result
    const mockInsertResult = { insertId: 1 };
    db.executeQuery.mockResolvedValueOnce([1]);
    db.executeQuery.mockResolvedValueOnce(mockInsertResult);

    const dayData = {
      destination_id: 1,
      day: {
        date: '2023-06-01',
        day_number: 1,
      },
    };

    const response = await request(app)
      .post('/save/day')
      .send(dayData)
      .set('Authorization', `Bearer ${mockToken}`);

    const expectedDay = new Day({
      day_id: mockInsertResult.insertId,
      date: new Date(dayData.day.date),
      day_number: dayData.day.day_number,
      activities: [],
    });

    jsonDay = JSON.parse(JSON.stringify(expectedDay));

    expect(response.status).toBe(200);
    expect(response.body).toEqual({ day: jsonDay });
  });
});

describe('POST /save/activity', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should return 400 if activity name is missing', async () => {
    const activityData = {
      day_id: 1,
      activity: {
        activity_name: '',
        activity_time: '10:00',
      },
    };

    const response = await request(app)
      .post('/save/activity')
      .send(activityData)
      .set('Authorization', `Bearer ${mockToken}`);

    expect(response.status).toBe(400);
    expect(response.body).toEqual({ message: 'Activity name is required' });
  });

  it('should return 400 if activity time is missing', async () => {
    const activityData = {
      day_id: 1,
      activity: {
        activity_name: 'Test Activity',
        activity_time: '',
      },
    };

    const response = await request(app)
      .post('/save/activity')
      .send(activityData)
      .set('Authorization', `Bearer ${mockToken}`);

    expect(response.status).toBe(400);
    expect(response.body).toEqual({ message: 'Invalid activity time. Please use the format hh:mm' });
  });

  it('should return 400 if activity time format is invalid', async () => {
    const activityData = {
      day_id: 1,
      activity: {
        activity_name: 'Test Activity',
        activity_time: 'invalid-time-format',
      },
    };

    const response = await request(app)
      .post('/save/activity')
      .send(activityData)
      .set('Authorization', `Bearer ${mockToken}`);

    expect(response.status).toBe(400);
    expect(response.body).toEqual({ message: 'Invalid activity time. Please use the format hh:mm' });
  });

  it('should return 400 if the day is not found', async () => {
    // Mock the database query result
    db.executeQuery.mockResolvedValueOnce([]);

    const activityData = {
      day_id: 1,
      activity: {
        activity_name: 'Test Activity',
        activity_time: '10:00',
      },
    };

    const response = await request(app)
      .post('/save/activity')
      .send(activityData)
      .set('Authorization', `Bearer ${mockToken}`);

    expect(response.status).toBe(400);
    expect(response.body).toEqual({ message: 'Day does not exist' });
  });

  it('should save a new activity and return the activity object', async () => {
    // Mock the database query result
    const mockInsertResult = { insertId: 1 };
    db.executeQuery.mockResolvedValueOnce([1]);
    db.executeQuery.mockResolvedValueOnce(mockInsertResult);

    const activityData = {
      day_id: 1,
      activity: {
        activity_name: 'Test Activity',
        activity_time: '10:00',
      },
    };

    const response = await request(app)
      .post('/save/activity')
      .send(activityData)
      .set('Authorization', `Bearer ${mockToken}`);

    const expectedActivity = new Activity({
      activity_id: mockInsertResult.insertId,
      activity_name: activityData.activity.activity_name,
      activity_url: '',
      activity_note: '',
      activity_time: activityData.activity.activity_time,
      activity_eta: 0,
    });

    expect(response.status).toBe(200);
    expect(response.body).toEqual({ day_id: 1, activity: expectedActivity });
  });
});

describe('POST /update/activity', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should return 400 if activity ID is missing', async () => {
    const activityData = {
      activity: {
        activity_id: '',
        activity_name: 'Updated Activity',
        activity_time: '10:00',
      },
    };

    const response = await request(app)
      .post('/update/activity')
      .send(activityData)
      .set('Authorization', `Bearer ${mockToken}`);

    expect(response.status).toBe(400);
    expect(response.body).toEqual({ message: 'Activity ID is required' });
  });

  it('should return 400 if activity name is missing', async () => {
    const activityData = {
      activity: {
        activity_id: 1,
        activity_name: '',
        activity_time: '10:00',
      },
    };

    const response = await request(app)
      .post('/update/activity')
      .send(activityData)
      .set('Authorization', `Bearer ${mockToken}`);

    expect(response.status).toBe(400);
    expect(response.body).toEqual({ message: 'Activity name is required' });
  });

  it('should return 400 if activity time is missing', async () => {
    const activityData = {
      activity: {
        activity_id: 1,
        activity_name: 'Updated Activity',
        activity_time: '',
      },
    };

    const response = await request(app)
      .post('/update/activity')
      .send(activityData)
      .set('Authorization', `Bearer ${mockToken}`);

    expect(response.status).toBe(400);
    expect(response.body).toEqual({ message: 'Invalid activity time. Please use the format hh:mm' });
  });

  it('should return 400 if activity time format is invalid', async () => {
    const activityData = {
      activity: {
        activity_id: 1,
        activity_name: 'Updated Activity',
        activity_time: 'invalid-time-format',
      },
    };

    const response = await request(app)
      .post('/update/activity')
      .send(activityData)
      .set('Authorization', `Bearer ${mockToken}`);

    expect(response.status).toBe(400);
    expect(response.body).toEqual({ message: 'Invalid activity time. Please use the format hh:mm' });
  });

  it('should update an existing activity and return the updated activity object', async () => {
    // Mock the database query result
    const mockUpdateResult = { affectedRows: 1 };
    db.executeQuery.mockResolvedValueOnce([1]);
    db.executeQuery.mockResolvedValueOnce(mockUpdateResult);

    const activityData = {
      activity: {
        activity_id: 1,
        activity_name: 'Updated Activity',
        activity_url: 'http://example.com',
        activity_note: 'Updated Note',
        activity_time: '10:00',
        activity_eta: 60,
      },
    };

    const response = await request(app)
      .post('/update/activity')
      .send(activityData)
      .set('Authorization', `Bearer ${mockToken}`);

    const expectedActivity = new Activity({
      activity_id: activityData.activity.activity_id,
      activity_name: activityData.activity.activity_name,
      activity_url: activityData.activity.activity_url,
      activity_note: activityData.activity.activity_note,
      activity_time: activityData.activity.activity_time,
      activity_eta: activityData.activity.activity_eta,
    });

    expect(response.status).toBe(200);
    expect(response.body).toEqual({ activity: expectedActivity });
  });

  it('should return 404 if activity ID does not exist', async () => {
    // Mock the database query result
    const mockUpdateResult = { affectedRows: 0 };
    db.executeQuery.mockResolvedValueOnce([]);

    const activityData = {
      activity: {
        activity_id: 999, // Non-existing ID
        activity_name: 'Updated Activity',
        activity_time: '10:00',
      },
    };

    const response = await request(app)
      .post('/update/activity')
      .send(activityData)
      .set('Authorization', `Bearer ${mockToken}`);

    expect(response.status).toBe(400);
    expect(response.body).toEqual({ message: 'Activity does not exist' });
  });
});

describe('POST /delete/activity', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should return 500 if activity ID is missing', async () => {
    
    db.executeQuery.mockRejectedValueOnce(new Error('Database error'));

    const response = await request(app)
      .post('/delete/activity')
      .send({})
      .set('Authorization', `Bearer ${mockToken}`);

    expect(response.status).toBe(500);
    expect(response.text).toBe('Internal Server Error');
  });

  it('should delete an existing activity and return success message', async () => {
    // Mock the database query result
    const mockDeleteResult = { affectedRows: 1 };
    db.executeQuery.mockResolvedValueOnce(mockDeleteResult);

    const activityId = 1;

    const response = await request(app)
      .post('/delete/activity')
      .send({ activity_id: activityId })
      .set('Authorization', `Bearer ${mockToken}`);

    expect(response.status).toBe(200);
    expect(response.body).toEqual({ 'Deleted Activity ': activityId });
  });

  it('should return 500 if activity ID does not exist', async () => {
    // Mock the database query result
    const mockDeleteResult = { affectedRows: 0 };
    db.executeQuery.mockRejectedValueOnce(new Error('Database error'));

    const nonExistingActivityId = 999; // Non-existing ID

    const response = await request(app)
      .post('/delete/activity')
      .send({ activity_id: nonExistingActivityId })
      .set('Authorization', `Bearer ${mockToken}`);

    expect(response.status).toBe(500);
    expect(response.text).toBe('Internal Server Error');
  });

  it('should return 500 if database query fails', async () => {
    // Mock the database query result
    db.executeQuery.mockRejectedValueOnce(new Error('Database error'));

    const activityId = 1;

    const response = await request(app)
      .post('/delete/activity')
      .send({ activity_id: activityId })
      .set('Authorization', `Bearer ${mockToken}`);

    expect(response.status).toBe(500);
    expect(response.text).toBe('Internal Server Error');
  });
});

afterAll(() => {
    app.close();
});