const { expressjwt } = require('express-jwt');

module.exports = jwt;

function jwt() {
    const { secret } = {secret: 'llamallamallama'}
    return expressjwt({ secret, algorithms: ['HS256'] }).unless({
        path: [
            // public routes that don't require authentication
            '/users/authenticate'
        ]
    });
}
