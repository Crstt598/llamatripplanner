import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { LlamaApiComponent } from './llama-api/llama-api.component';
import { PlannerComponent } from './planner/planner.component';
import { User } from '@app/classes/user';

import { AuthenticationService } from '@app/services';
import { Trip } from '@app/classes/trip';
import { DataService } from '@app/services/dataService/data.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-main',
  standalone: true,
  imports: [LlamaApiComponent, PlannerComponent],
  templateUrl: './main.component.html',
  styleUrl: './main.component.scss'
})
export class MainComponent implements AfterViewInit {

  trip : Trip = {} as Trip;
  @ViewChild(PlannerComponent) plannerComponent!: PlannerComponent;
  @ViewChild(LlamaApiComponent) LlamaApiComponent!: LlamaApiComponent;

  constructor(public dataService: DataService, private route: ActivatedRoute) {    
  }

  ngAfterViewInit(): void {
    this.route.queryParams.subscribe(async params => {
      if (params['trip_id']) {
        console.log("Recovering trip with id: ",params['trip_id']);
        this.trip = await this.plannerComponent.recoverTrip(params['trip_id']);
        this.LlamaApiComponent.trip = this.trip;
        this.plannerComponent.trip = this.trip;
        console.log(this.trip, this.LlamaApiComponent.trip, this.plannerComponent.trip);
        
        this.LlamaApiComponent.resetContext();
        this.LlamaApiComponent.greetReturningUser();
      }else{
        this.LlamaApiComponent.hideShowSpinner(false);
      }
    });
  }
}
