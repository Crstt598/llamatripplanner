import { Component, Input} from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import {prompts, chatStates} from './prompts/prompts';
import OpenAI from 'openai';
import { PlannerComponent } from '../planner/planner.component';
import { DataService } from '../../services/dataService/data.service';
import { Trip, Destination, Accommodation, Day, Activity, Expense, Note } from '../../classes/trip';
import { environment } from '@environments/environment';
import { AuthenticationService } from '@app/services/authentication.service';

@Component({
  selector: 'app-llama-api',
  standalone: true,
  imports: [CommonModule, PlannerComponent, HttpClientModule],
  templateUrl: './llama-api.component.html',
  styleUrl: './llama-api.component.scss'
})

export class LlamaApiComponent{
  private openai: any;  
  chatResults: ChatBubble[] = [];  
  userMessage:string = '';
  chatResult = '';
  conversationHistory: { role: string; content: string }[] = [{
    role: 'system',
    content: prompts.initialPrompts[0].replaceAll("$todayDate$", new Date().toISOString().split('T')[0])
  }];
  chatState = chatStates[0];  
  //trip: any = {};

  @Input() trip!: Trip;

  constructor(public dataService: DataService, private authenticationService: AuthenticationService) {

    const user = this.authenticationService.userValue;
    
    this.openai = new OpenAI({
      //apiKey: 'sk-no-key-required',
      apiKey: user?.token,
      baseURL: `${environment.apiUrl}/v1`,
      dangerouslyAllowBrowser: true
    });
  }

  onChatClick(event: any) {
    const chatBubble: ChatBubble = {userResponse: this.userMessage, isUser: true};
    this.chatResults.push(chatBubble);
    this.chatWithLLM(this.userMessage)
      .then(response => console.log(response))
      .catch(error => console.error(error));
  }

  parseResponse(response: string) {
    try {      
      console.log(response);
      response = response.replace(/(^.*:\s*{)/, '{');
      const data = JSON.parse(response);      
      const chatBubble: ChatBubble = {userResponse: data.userResponse, isUser: false};
      this.chatResults.push(chatBubble);
      return data;

    } catch (error) {
      //console.error("Error parsing JSON:", error);
      const chatBubble: ChatBubble = {userResponse: response, isUser: false};
      this.chatResults.push(chatBubble);
    }
  }

  async parseInitialResponse(response: string) {
    var data = this.parseResponse(response);
    PlannerComponent.prototype.decodeTripToPlanner(data, this.dataService, this.trip)
      .then(async (resolvedTrip: Trip) => {
        this.trip = resolvedTrip;
        console.log('initPlanner - Resolved Trip:', this.trip);        
        const processDestinations = async () => { // Add wait to process to run destinations in sequence               
          for (const destination of this.trip.destinations) {            
            var conversationHistory = [... this.conversationHistory];   
            var prompt = prompts.activityPrompts[0];   
            conversationHistory.push( { role: 'system', content: prompt});         
            await this.processDestination(destination, conversationHistory);
          }
          console.log('All destinations processed');
          this.resetContext();
        };

        await processDestinations();
        
      })
      .catch(error => {
        console.error('Error initializing planner:', error);
      });     
  }

  
  /**
   * Shows or hides the spinner based on the provided boolean value.
   * @param show - A boolean value indicating whether to show or hide the spinner.
   * True shows the spinner, false hides it.
   */
  hideShowSpinner(show: boolean) {
    const spinners = document.getElementsByClassName('spinnerToHide');
    for (let i = 0; i < spinners.length; i++) {
      const spinner = spinners[i] as HTMLElement;
      if (show) {
        spinner.setAttribute('style', 'display: flex !important');
        //console.log('Show spinner', spinner);
      } else {
        spinner.setAttribute('style', 'display: none !important');
        //console.log('Hide spinner', spinner);
      }
    }
  }

  resetContext(trip?: Trip){
    this.chatState = 'info';
    if(trip){
      this.trip = trip;
    }
    var context: { destination: any, days: { day: any, activities: any[] }[] }[] = [];
    for (const destination of this.trip.destinations) {
      context.push({destination: destination.destination_name, days: []})
      for (const day of destination.days) {
        context[context.length - 1].days.push({day: day.day_number, activities: []})
        for (const activity of day.activities) {
          context[context.length - 1].days[context[context.length - 1].days.length - 1].activities.push({name: activity.activity_name, time: activity.activity_time});          
        }
      }
    }
    console.log(context);
    var content = prompts.infoPrompts[0] + JSON.stringify(context);
    this.conversationHistory = [{ role: 'system', content: content }];
  }

  async greetReturningUser(){
    var greetMessage = prompts.greetPrompts[0];
    this.conversationHistory.push({ role: 'system', content: greetMessage });    
    this.parseResponse(await this.sendMessage())    
  }

  async parseResponseActivities(response: any) {
    var data = this.parseResponse(response);
    await PlannerComponent.prototype.parseActivities(data, this.dataService)
      .then((resolvedTrip: Trip) => {
        this.trip = resolvedTrip;
        console.log('parseActivities - Resolved Trip:', this.trip);
      })
      .catch(error => {
        console.error('Error parsing response activities:', error);
      });
  }

  onInputChange(event: any) {
    this.userMessage = event.target.value;
  }

  clearChatInput(){
    this.userMessage = '';
    const destinationElement = document.getElementById("chatInput") as HTMLInputElement;
      if (destinationElement) {
        destinationElement.value = '';
      }
  }

  async chatWithLLM(userMessage: string): Promise<string> {
    this.conversationHistory.push({ role: 'user', content: userMessage });
    this.clearChatInput();
    console.log(this.chatState, this.trip);
    switch (this.chatState) {
      case 'initial':        
        this.parseInitialResponse(await this.sendMessage())

        this.chatState = 'activity';
        break;
      case 'info':
        this.parseResponse(await this.sendMessage())
        break;
    }    
    return '';
  }

  async processDestination(dest: Destination, conversationHistory: any[]) {
    // Iterate over each day in the destination
    for (const day of dest.days) {
      await this.processDay(dest, day, conversationHistory);
    }
  }

  async processDay(dest: Destination, day: Day, conversationHistory: any[]) {
    try {
      // Make async request for the day
      var prompt = `Plan some activities for day ${day.day_number} in ${dest.destination_name}.`;       
      conversationHistory.push( { role: 'user', content: prompt});   
      var Response = await this.sendMessage(conversationHistory); 
      conversationHistory.push({ role: 'system', content: Response });
      await this.parseResponseActivities(Response)
      console.log(`Processed day ${day.day_id}`);
    } catch (error) {
      console.error(`Error processing day ${day.day_id}: ${error}`);
    }
  }

  async parseActivities(response: any) {
    try {      
      console.log(response);
      response = response.replace(/(^.*:\s*{)/, '{');
      const data = JSON.parse(response);      
      
      const chatBubble: ChatBubble = {userResponse: data.userResponse, isUser: false};
      this.chatResults.push(chatBubble);

      await PlannerComponent.prototype.parseActivities(data, this.dataService)
        .then((resolvedTrip: Trip) => {
          this.trip = resolvedTrip;
          console.log('parseActivities - Resolved Trip:', this.trip);
        })
        .catch(error => {
          console.error('Error initializing planner:', error);
        });

      console.log(this.trip);
    
    } catch (error) {
      console.error("Error parsing JSON:", error);
      const chatBubble: ChatBubble = {userResponse: response, isUser: false};
      this.chatResults.push(chatBubble);
    }
  }

  /*private async sendMessage(optionalMessages?: any[]) {
    this.hideShowSpinner(true);
    var messages = [...this.conversationHistory];
    if (optionalMessages) {
      messages = optionalMessages;
    }

    var Response = await this.openai.chat.completions.create({
      model: 'LLaMA_CPP',
      messages: messages
    })
      .then((completion: any) => completion.choices[0].message.content)
      .catch((error: any) => {
        console.error('Error:', error);
        throw error;
      });

    if(!optionalMessages){
      this.conversationHistory.push({ role: 'system', content: Response });
    }
    this.hideShowSpinner(false);
    return Response;
  }*/

  private async sendMessage(optionalMessages?: any[]) {
    this.hideShowSpinner(true);
    var messages = [...this.conversationHistory];
    if (optionalMessages) {
      messages = optionalMessages;
    }

    var Response = await this.openai.chat.completions.create({
      model: 'LLaMA_CPP',
      messages: messages
    })
      .then((completion: any) => completion.choices[0].message.content)
      .catch((error: any) => {
        console.error('Error:', error);
        throw error;
      });

    if(!optionalMessages){
      this.conversationHistory.push({ role: 'system', content: Response });
    }
    this.hideShowSpinner(false);
    return Response;
  }
}

interface ChatBubble {
  userResponse: string;
  isUser: boolean; // true if the user is the sender, false if it's the bot
}
