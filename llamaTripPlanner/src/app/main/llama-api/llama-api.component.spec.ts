import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LlamaApiComponent } from './llama-api.component';

describe('LlamaApiComponent', () => {
  let component: LlamaApiComponent;
  let fixture: ComponentFixture<LlamaApiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [LlamaApiComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(LlamaApiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
