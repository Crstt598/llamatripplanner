import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Renderer2, ViewContainerRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Trip, Destination, Accommodation, Day, Activity, Expense, Note } from '../../../classes/trip';
import { ActivityPopupComponent } from './activity-popup/activity-popup.component';
import { DataService } from '../../../services/dataService/data.service';

@Component({
  selector: 'app-planner-day-component',
  standalone: true,
  imports: [CommonModule, ActivityPopupComponent],
  templateUrl: './planner-day.component.html',
  styleUrl: './planner-day.component.scss'
})
export class PlannerDayComponent {
  day: Day = new Day();
  selectedActivity!: Activity;
  activityPopupRef!: any;
  dataService!: DataService;

  dayComponentChagneDecetorRef!: ChangeDetectorRef;

  //constructor() {}
  constructor(private renderer: Renderer2, private viewContainerRef: ViewContainerRef) { }

  activityDetail( activity: Activity, mode: string = 'Update') {
    console.log("Activity Detail: ", activity);
    this.selectedActivity = activity;
    if (!this.activityPopupRef) {
      this.activityPopupRef = this.viewContainerRef.createComponent(ActivityPopupComponent);
      
      this.activityPopupRef.instance.dayComponentChagneDecetorRef = this.dayComponentChagneDecetorRef;
      this.activityPopupRef.instance.dataService = this.dataService;
      
      this.activityPopupRef.instance.closePopup.subscribe(() => this.hideActivityPopup());
      this.activityPopupRef.instance.delActivity.subscribe((activity_id : number) => this.delActivity(activity_id));      
      this.activityPopupRef.instance.completedAddActivity.subscribe((activity : Activity) => this.completedAddActivity(activity));  
    }

    this.activityPopupRef.instance.activity = this.selectedActivity;
    this.activityPopupRef.instance.mode = mode;
    this.activityPopupRef.instance.day_id = this.day.day_id;

    this.renderer.setStyle(this.activityPopupRef.hostView.rootNodes[0], 'display', 'block');
    this.activityPopupRef.changeDetectorRef.detectChanges();
    this.activityPopupRef.instance.initForm();
  }

  hideActivityPopup() {
    this.renderer.setStyle(this.activityPopupRef.hostView.rootNodes[0], 'display', 'none');
  }

  delActivity(activity_id : number) {
    //console.log("Delete Activity: ", activity_id);
    this.day.activities = this.day.activities.filter(activity => activity.activity_id !== activity_id);
    this.dayComponentChagneDecetorRef.detectChanges();
  }

  completedAddActivity(activity: Activity) {
    console.log("Completed Add Activity: ", activity);
    this.day.activities.push(activity);
    this.dayComponentChagneDecetorRef.detectChanges();
  }

  addActivity() {
    //console.log("Add Activity");
    let activity = new Activity();
    //this.dayComponentChagneDecetorRef.detectChanges();
    this.activityDetail(activity, 'Add');
  }
}
