import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityPopupComponent } from './activity-popup.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Activity } from '@app/classes/trip';

describe('ActivityPopupComponent', () => {
  let component: ActivityPopupComponent;
  let fixture: ComponentFixture<ActivityPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ActivityPopupComponent, RouterTestingModule, HttpClientTestingModule]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ActivityPopupComponent);    
    component = fixture.componentInstance;
    //console.log("Test ActivityPopupComponent", component.activity);
    component.activity = new Activity({
      activity_id: 1,
      activity_name: 'test',
      activity_url: 'test',
      activity_note: 'test',
      activity_time: '10:00',
      activity_eta: '0'
    });
    
    component.ngOnInit();
    component.initHtmlElements();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
