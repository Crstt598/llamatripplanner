import { ChangeDetectorRef, Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { Activity } from '../../../../classes/trip';
import { ReactiveFormsModule, FormControl, Validators, FormGroup } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { DataService } from '../../../../services/dataService/data.service';

@Component({
  selector: 'app-activity-popup',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule],
  templateUrl: './activity-popup.component.html',
  styleUrl: './activity-popup.component.scss'
})
export class ActivityPopupComponent {

  @Input() activity!: Activity;
  @Input() day_id!: number;
  @Input() mode!: string;
  @Output() closePopup = new EventEmitter<void>();
  @Output() delActivity = new EventEmitter<number>();
  @Output() completedAddActivity = new EventEmitter<Activity>();

  form!: FormGroup;
  
  nameCtrl!: FormControl;
  urlCtrl!: FormControl;
  noteCtrl!: FormControl;
  timeCtrl!: FormControl;
  etaCtrl!: FormControl;

  formHtml!: HTMLFormElement;

  nameDiv!: HTMLElement;
  urlDiv!: HTMLElement;
  noteDiv!: HTMLElement;
  timeDiv!: HTMLElement;
  etaDiv!: HTMLElement;
  
  dayComponentChagneDecetorRef!: ChangeDetectorRef;
  dataService!: DataService;

  ngOnInit() {    
    this.createFormControls();
    this.createForm();
  }

  createFormControls() {
    this.nameCtrl = new FormControl('', Validators.required);
    this.urlCtrl = new FormControl('', Validators.pattern('^(https?://)?([\\w-]+\\.)+[\\w-]+(/[\\w-./?%&=]*)?$'));
    this.noteCtrl = new FormControl();
    this.timeCtrl = new FormControl('', Validators.pattern('\\d\\d:\\d\\d'));
    this.etaCtrl = new FormControl(); 
  }

  createForm() {
    this.form = new FormGroup({
      name: this.nameCtrl,
      url: this.urlCtrl,
      note: this.noteCtrl,
      time: this.timeCtrl,
      eta: this.etaCtrl
    });
  }

  initHtmlElements(){
    this.formHtml = document.getElementById('activityForm'+this.activity.activity_id) as HTMLFormElement;

    this.nameDiv = document.getElementById('nameDiv')!;
    this.urlDiv = document.getElementById('urlDiv')!;
    this.noteDiv = document.getElementById('noteDiv')!;
    this.timeDiv = document.getElementById('timeDiv')!;
    this.etaDiv = document.getElementById('etaDiv')!;
  }

  onClosePopup() {
    
    this.formHtml.classList.remove('was-validated');
    this.closePopup.emit();
  }

  initForm(){
    //console.log("Init Form: ", this.activity);
    
    this.nameCtrl.setValue(this.activity.activity_name);
    this.urlCtrl.setValue(this.activity.activity_url);
    this.noteCtrl.setValue(this.activity.activity_note);
    this.timeCtrl.setValue(this.activity.activity_time);
    this.etaCtrl.setValue(this.activity.activity_eta);

    this.initHtmlElements();
  }

  submitActivity(){
    //console.log("Update Activity: ", this.activity);
    this.form.markAllAsTouched();
    this.formHtml.classList.add('was-validated');
    if(this.form.status == 'VALID'){
      //console.log("Valid Form");
      this.activity.activity_name = this.nameCtrl.value;
      this.activity.activity_url = this.urlCtrl.value;
      this.activity.activity_note = this.noteCtrl.value;
      this.activity.activity_time = this.timeCtrl.value;
      this.activity.activity_eta = this.etaCtrl.value;

      
      if (this.mode === 'Add') {
        this.dataService.saveActivity(this.day_id, this.activity).subscribe(
          activity => {
            this.activity.activity_id = activity.activity_id;
            this.completedAddActivity.emit(this.activity);
          }        
        );
      } else {
        this.dataService.updateActivity(this.activity).subscribe();
      }

      this.dayComponentChagneDecetorRef.detectChanges();
      this.onClosePopup();
    }
  }

  deleteActivity(){
    if (confirm("Are you sure you want to delete this activity?") == true) {
      //console.log("Delete Activity Popup: ", this.activity);
      this.dataService.deleteActivity(this.activity.activity_id).subscribe();
      this.dayComponentChagneDecetorRef.detectChanges();
      this.delActivity.emit(this.activity.activity_id);
      this.onClosePopup();
    }    
  }

}
