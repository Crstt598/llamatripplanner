import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlannerComponent } from './planner.component';
import { DataService } from '@app/services/dataService/data.service';
import { PlannerDayComponent } from './planner-day/planner-day.component';
import { HttpClientModule } from '@angular/common/http';

describe('PlannerComponent', () => {
  let component: PlannerComponent;
  let fixture: ComponentFixture<PlannerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PlannerComponent, HttpClientModule],
      providers: [DataService]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PlannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
