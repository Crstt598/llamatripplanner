import { Component, ComponentRef, Input, OnInit, createComponent } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Trip, Destination, Accommodation, Day, Activity, Expense, Note } from '../../classes/trip';
import { PlannerDayComponent } from './planner-day/planner-day.component';
import { PlannerDestinationComponent } from './planner-destination/planner-destination.component';
import { bootstrapApplication } from '@angular/platform-browser';
import { AppComponent } from '../../app.component';
import { DataService } from '../../services/dataService/data.service';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-planner',
  standalone: true,
  imports: [CommonModule, PlannerDayComponent],
  templateUrl: './planner.component.html',
  styleUrl: './planner.component.scss'
})

export class PlannerComponent{  

  constructor(public dataService: DataService){        
  }

  @Input() trip: any = {};
  
  isTripInitialized: boolean = false;  
  daysComponentRefs : ComponentRef<any>[] = [];
  destComponentRefs : ComponentRef<any>[] = [];
  
  /**
   * Decodes trip data and updates the planner component.
   * @param data - The trip data to decode.
   * @param dataService - The data service to use for saving the trip.
   * @param trip - The trip object to update.
   * @returns A promise that resolves to the updated trip object.
   */
public async decodeTripToPlanner(data: any, dataService: DataService, trip: any): Promise<Trip> {
  this.trip = trip;
  this.dataService = dataService;

  this.trip = new Trip({ trip_name: data.tripName, description: data.description, location: data.location, start_date: data.startDate, end_date: data.endDate, destinations: data.itinerary });

  this.setValueOfInput("start_date", this.trip.start_date);
  this.setValueOfInput("end_date", this.trip.end_date);

  try {
    const response = await dataService.saveTrip(this.trip).toPromise();
    console.log(response);

    this.trip.trip_id = response.trip.trip_id;

    const destinationElement = document.getElementById('tripTile');
    if (destinationElement) {
      destinationElement.innerHTML = this.trip.trip_name;
    }

    await this.createPlanner(this.trip.start_date, dataService);

    console.log('Trip saved successfully:', this.trip);
  } catch (error) {
    console.error('Error:', error);
    throw error; // Rethrow the error to be caught by the caller
  }

  return this.trip;
}

  /**
     * Sets the value of an input element.
     * @param input - The ID of the input element.
     * @param value - The value to set.
     */
  setValueOfInput(input: string, value: any) {
    const startDateElement = document.getElementById(input) as HTMLInputElement;
    if (startDateElement && value) {
      try{
        startDateElement.value = value.toISOString().split('T')[0];
      }catch(e) {
        startDateElement.value = value;
      }      
    }
  }

  saveTrip(data: any, dataService:DataService): void {
    
    dataService.saveTrip(data).subscribe((response: any) => {
      console.log(response);
      return response;
    }, (error: any) => {
      console.error('Error:', error);
    });
  }

  saveTripWhole(data: any, dataService:DataService): void {
    
    dataService.saveTripWhole(data).subscribe((response: any) => {
      console.log(response);
      return response;
    }, (error: any) => {
      console.error('Error:', error);
    });
  }

  public async parseActivities(data: any, dataService: DataService): Promise<Trip> {
    const saveActivityObservables: Observable<any>[] = [];
  
    this.trip.destinations.forEach((destination: any, index: number) => {
      destination.days.forEach((day: any, i: number) => {
        if (day.day_number == data.dayNumber && destination.destination_name == data.destination) {
          data.activities.forEach((activity: any, index: number) => {
            // Pushing activity saving observables to the array
            activity = new Activity(activity);
            const saveActivity$ = dataService.saveActivity(day.day_id, activity).pipe(
              map((response: any) => {
                return response; // Return the response for this activity
              })
            );
            saveActivityObservables.push(saveActivity$);
          });
        }
      });
    });
  
    // Execute all saveActivityObservables concurrently using forkJoin
    forkJoin(saveActivityObservables).subscribe({
      next: (responses: any[]) => {

        this.trip.destinations.forEach((destination: any) => {
          destination.days.forEach((day: any) => {
            responses.forEach((response: any, index: number) => {
              if (day.day_id == response.day_id){
                day.activities[index] = new Activity(response.activity);
              }
            });
          });
        });

        // Update component view
        this.refreshAllDays();

        //console.log('All activities saved successfully:', responses);
      },
      error: (error: any) => {
        console.error('An error occurred while saving activities:', error);
        // Handle error
      }
    });
  
    // Return the updated trip
    return this.trip;
  }
  
  private refreshAllDays() {
    this.daysComponentRefs.forEach((dayComponentRef: ComponentRef<any>) => {
      dayComponentRef.changeDetectorRef.detectChanges();
    });
  }

  public async recoverTrip(trip_id: number): Promise<Trip> {
    return new Promise<Trip>((resolve, reject) => {
      this.dataService.getTrip(trip_id).subscribe((response: any) => {
        this.trip = new Trip(response.trip);
  
        const destinationElement = document.getElementById('tripTile');
        if (destinationElement) {
          destinationElement.innerHTML = this.trip.trip_name;
        }
  
        this.populatePlanner();
        resolve(this.trip); // Resolve the Promise with the trip
      }, (error: any) => {
        console.error('Error:', error);
        reject(error); // Reject the Promise if there's an error
      });
    });
  }

  populatePlanner(){

    this.setValueOfInput("start_date", this.trip.start_date.toISOString().split('T')[0]);
    this.setValueOfInput("end_date", this.trip.end_date.toISOString().split('T')[0]);

    this.attachDestinations(this.trip.destinations);

    this.trip.destinations.forEach((destination: any, index: number) => {
      destination.days.forEach((day: Day) => {
        this.daysComponentRefs.push(this.createPlannerDay(destination.destination_id, day.day_id, day));
      });           
    });    
  }

  /**
   * Creates planner day components for each destination and attaches them to the DOM.
   * 
   * @param startDate - The start date for the trip.
   * @param daysComponentRefs - An array of existing component references.
   * @returns An array of component references for the created planner day components.
   */
  async createPlanner(startDate: Date, dataService: DataService): Promise<void> {
    let destStartDate = new Date(startDate);
  
    // Iterate over each destination
    for (let i = 0; i < this.trip.destinations.length; i++) {
      let destination = this.trip.destinations[i];
      const numberOfDays = destination.numberOfDays;
  
      destination = new Destination({
        destination_id: i,
        destination_name: destination.destination,
        arrival_date: new Date(new Date(destStartDate).toISOString()),
        departure_date: new Date(new Date(destStartDate.getTime() + numberOfDays * 24 * 60 * 60 * 1000).toISOString()),
        days: []
      });  
      destStartDate.setDate(destStartDate.getDate() + numberOfDays);  
  
      // Save destination and days
      try {
        const response = await dataService.saveDestination(this.trip.trip_id, destination).toPromise();
        destination.destination_id = response.destination.destination_id;
  
        const dayDate = new Date(destination.arrival_date);
  
        // Prepare array to hold observables for saving days
        const saveDayObservables: Observable<any>[] = [];
  
        // Iterate over each day
        this.range(0, numberOfDays).forEach((i: number) => {
          const dayNumber = Math.floor((dayDate.getTime() - startDate.getTime()) / (1000 * 60 * 60 * 24)) + 1;
            
          const day: Day = new Day({
            day_id: i,
            date: new Date(dayDate),
            day_number: dayNumber,
            activities: []
          });
  
          dayDate.setDate(dayDate.getDate() + 1);
          destination.days[i] = day;
  
          // Convert asynchronous call to save a day into an observable
          const saveDay$ = dataService.saveDay(destination.destination_id, day).pipe(
            map((response: any) => {
              day.day_id = response.day.day_id;
              console.log(day);
              return response; // Return the response for this day
            })
          );
  
          // Add the observable to the array
          saveDayObservables.push(saveDay$);
        });
  
        // Execute all saveDayObservables concurrently using forkJoin
        const responses = await forkJoin(saveDayObservables).toPromise();
  
        // Log success and create planner days
        console.log('All days saved successfully:', responses);
        
        this.attachDestination(destination, i);

        destination.days.forEach((day: any, i: number) => {
          if (!this.daysComponentRefs) {
            this.daysComponentRefs = [this.createPlannerDay(destination.destination_id, day.day_id, day)];
          } else {
            this.daysComponentRefs.push(this.createPlannerDay(destination.destination_id, day.day_id, day));
          }
        });

        this.trip.destinations[i] = destination;
  
      } catch (error) {
        console.error(`Error saving destination ${destination.destination_id}:`, error);
        // Handle error
      }
    }
  }  

  attachDestination(destination: Destination, optionalIndex?: number) {
    if (!this.destComponentRefs) {
      this.destComponentRefs = [this.createDestination(destination, optionalIndex)];
    } else {
      this.destComponentRefs.push(this.createDestination(destination, optionalIndex));
    }

    const destinationElement = document.getElementById('destinationTitle' + destination.destination_id);
    if (destinationElement) {
      destinationElement.innerHTML = destination.destination_name;
    } else {
      console.error("Destination element not found for destination: " + destination.destination_id);
    }         
  }

  attachDestinations(destinations: any[]) {
    destinations.forEach((destination: any, i: number) => {
      //this.destComponentRefs.push(this.createDestination(destination));
      if (!this.destComponentRefs) {
        this.destComponentRefs = [this.createDestination(destination, i)];
      } else {
        this.destComponentRefs.push(this.createDestination(destination, i));
      }

      const destinationElement = document.getElementById('destinationTitle' + destination.destination_id);
      if (destinationElement) {
        destinationElement.innerHTML = destination.destination_name;
      } else {
        console.error("Destination element not found for destination: " + destination.destination_id);
      }                 
    });
  }

  /**
   * Creates a planner day component and attaches it to the DOM.
   * 
   * @param i - The destination index.
   * @param k - The day index.
   * @param day - The day object.
   * @returns The reference to the created planner day component.
   */
  createPlannerDay( i: number, k: number, day: Day) : ComponentRef<any> {
    var dayComponentRef: ComponentRef<any> = {} as ComponentRef<any>;
    const hostElement = document.getElementById('plannerDayContainer' + i + '' + k);
    if (hostElement) {
      dayComponentRef = createComponent(PlannerDayComponent, { hostElement, environmentInjector });

      applicationRef.attachView(dayComponentRef.hostView);
      dayComponentRef.instance.day = day;
      dayComponentRef.instance.dataService = this.dataService;
      dayComponentRef.instance.dayComponentChagneDecetorRef = dayComponentRef.changeDetectorRef;
      dayComponentRef.changeDetectorRef.detectChanges();

    }else{
      console.error("Host element not found for destination: " + i + " day: " + k);
    }

    return dayComponentRef;
  }
  

  createDestination(destination: Destination, optionalIndex?: number): ComponentRef<any> {
    let destinationIndex = destination.destination_id;

    if (optionalIndex !== undefined) {
      destinationIndex = optionalIndex;
    }

    var destComponentRef: ComponentRef<any> = {} as ComponentRef<any>;
    
    const hostElement = document.getElementById('destinationContainer' + destinationIndex);
    if (hostElement) {
      
        destComponentRef = createComponent(PlannerDestinationComponent, { hostElement, environmentInjector });
        
        applicationRef.attachView(destComponentRef.hostView);
        
        destComponentRef.instance.destination = destination;        
        destComponentRef.instance.editDestination.subscribe((dest : Destination) => this.editDestination(dest));
        console.log(destComponentRef.instance.destination);
        destComponentRef.changeDetectorRef.detectChanges();
    } else {
      console.error("Host element not found for destinationContainer: " + destinationIndex);
    }
    return destComponentRef;
  }

  /**
   * Generates an array of numbers within a specified range.
   *
   * @param start - The starting number of the range (inclusive).
   * @param stop - The ending number of the range (exclusive).
   * @param step - The increment value between numbers in the range (default: 1).
   * @returns An array of numbers within the specified range.
   */
  range(start: number, stop: number, step: number = 1): number[] {
    const result = [];
    for (let i = start; i < stop; i += step) {
      result.push(i);
    }

    //console.log(result, start, stop, step);
    return result;
  }

  editDestination(destination: Destination) {
    console.log('Destination edited:', destination);
  }
}

let applicationRef: any;
let environmentInjector: any;

(async () => {
  applicationRef = await bootstrapApplication(AppComponent);
  environmentInjector = applicationRef.injector;
})();