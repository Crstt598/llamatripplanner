import { Component, EventEmitter, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Trip, Destination, Accommodation, Day, Activity, Expense, Note } from '../../../classes/trip';
import { PlannerComponent } from '../planner.component';

@Component({
  selector: 'app-planner-destination',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './planner-destination.component.html',
  styleUrl: './planner-destination.component.scss'
})
export class PlannerDestinationComponent {
  destination: Destination = new Destination();
  constructor() {
    
  }
  
  @Output() editDestination = new EventEmitter<Destination>();

  onContentEdited(event: Event) {
    const target = event.target as HTMLHeadingElement;
    const newContent = target.textContent;
    // Do something with the new content, e.g., update the destination name
    //this.destination.destination_name = newContent;
    console.log('New content:', newContent);    
    this.editDestination.emit(this.destination);
  }

  /**
   * Generates an array of numbers within a specified range.
   *
   * @param start - The starting number of the range (inclusive).
   * @param stop - The ending number of the range (exclusive).
   * @param step - The increment value between numbers in the range (default: 1).
   * @returns An array of numbers within the specified range.
   */
  range(start: number, stop: number, step: number = 1): number[] {
    const result = [];
    for (let i = start; i < stop; i += step) {
      result.push(i);
    }

    //console.log(result, start, stop, step);
    return result;
  }
}
