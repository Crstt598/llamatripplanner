import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlannerDestinationComponent } from './planner-destination.component';

describe('PlannerDestinationComponent', () => {
  let component: PlannerDestinationComponent;
  let fixture: ComponentFixture<PlannerDestinationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PlannerDestinationComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PlannerDestinationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
