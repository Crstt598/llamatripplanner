import { Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { AuthGuard } from './helpers/auth.guard';
import { MainComponent } from './main/main.component';
import { HomeComponent } from './home/home.component';
import { LogoutComponent } from './auth/logout/logout.component';

export const routes: Routes = [
    { path: '', component: HomeComponent, canActivate: [AuthGuard] },    
    { path: 'planner', component: MainComponent, canActivate: [AuthGuard] },   
    { path: 'logout', component: LogoutComponent, canActivate: [AuthGuard] },   
    { path: 'login', component: LoginComponent },
    { path: '**', redirectTo: '' }
];