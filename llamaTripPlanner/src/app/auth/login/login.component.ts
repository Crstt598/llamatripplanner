import { Component, OnInit } from '@angular/core';
import { NgIf } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { Form, FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AuthenticationService } from '@app/services';

@Component({ 
  standalone: true,
  templateUrl: 'login.component.html',
  imports: [ReactiveFormsModule, NgIf]
})
export class LoginComponent implements OnInit {
    loginForm!: FormGroup;

    username!: FormControl;
    password!: FormControl;
    
    loading = false;
    submitted = false;
    error = '';

    constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router, private authenticationService: AuthenticationService) { 
      //console.log('LoginComponent#constructor called');
      // redirect to home if already logged in
      if (this.authenticationService.userValue) { 
          this.router.navigate(['/']);
      }
    }

    ngOnInit() {    
      this.createFormControls();
      this.createForm();
    }
  
    createFormControls() {
      this.username = new FormControl('', Validators.required);
      this.password = new FormControl('', Validators.required);
    }
  
    createForm() {
      this.loginForm = new FormGroup({
        username: this.username,
        password: this.password
      });
    }

    // convenience getter for easy access to form fields
    get f() : any { return this.loginForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        this.error = '';
        this.loading = true;
        this.authenticationService.login(this.f.username.value, this.f.password.value)
            .pipe(first())
            .subscribe({
                next: () => {
                    // get return url from route parameters or default to '/'
                    const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
                    this.router.navigate([returnUrl]);
                },
                error: error => {
                    this.error = error;
                    this.loading = false;
                }
            });
    }
}