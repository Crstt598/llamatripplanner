import { Component } from '@angular/core';
import { User } from '@app/classes/user';
import { AuthenticationService } from '@app/services';

@Component({
  selector: 'app-logout',
  standalone: true,
  imports: [],
  templateUrl: './logout.component.html',
  styleUrl: './logout.component.scss'
})
export class LogoutComponent {

  user!: User;

  constructor(private authenticationService: AuthenticationService) { 
    this.authenticationService.user.subscribe(x => this.user = x!);
  }

  logout() {
    this.authenticationService.logout();
  }
}
