export {
    Trip,
    Destination,
    Accommodation,
    Day,
    Activity,
    Expense,
    Note    
}

abstract class ItineraryItem {

  public constructor(init?: Partial<ItineraryItem>) {
      Object.assign(this, init);
  }

  abstract getInfo(): string;
}

class Trip extends ItineraryItem{
  trip_id: number = -1;
  user_id?: number;
  trip_name: string = '';
  start_date?: Date;
  end_date?: Date;
  location?: string;
  description?: string;
  destinations: Destination[] = [];
  accommodations: Accommodation[] = [];
    
  public constructor(init?:Partial<Trip>) {    
    super(init);    
    Object.assign(this, init);
    if (init) {
      if (init.destinations && init.destinations.length > 0) {
        this.destinations = init.destinations.map(destination => new Destination(destination));
      }
      // Convert start_date and end_date strings to Date objects
      if (init.start_date) {
        this.start_date = new Date(init.start_date);
      }
      if (init.end_date) {
        this.end_date = new Date(init.end_date);
      }
    }
  }

  getInfo(): string {
    return `Trip: ${this.trip_name}`;
  }
}
  
class Destination extends ItineraryItem {
  destination_id: number = -1;
  destination_name: string = '';
  arrival_date?: Date;
  departure_date?: Date;
  description?: string;
  days: Day[] = [];

  public constructor(init?: Partial<Destination>) {
    super(init);    
    Object.assign(this, init);
    if (init) {
      if (init.days && init.days.length > 0) {
        this.days = init.days.map(day => new Day(day));
      }
      if (init.arrival_date) {
          this.arrival_date = new Date(init.arrival_date);
      }
      if (init.departure_date) {
          this.departure_date = new Date(init.departure_date);
      }
    }
  }

  getInfo(): string {
      return `Destination: ${this.destination_name}`;
  }
}

class Day extends ItineraryItem {
  day_id: number = -1;
  date?: Date;
  destination?: string;
  day_number?: number;
  description?: string;
  activities: Activity[] = [];
  expenses?: Expense[];
  notes?: Note[];

  public constructor(init?: Partial<Day>) {
    super(init);      
    Object.assign(this, init);
    if (init) {
      if (init.activities && init.activities.length > 0) {
        this.activities = init.activities.map(activity => new Activity(activity));
      }
      if (init.date) {
          this.date = new Date(init.date);
      }
    }
  }

  getInfo(): string {
      return `Day ${this.day_number}: ${this.description}`;
  }
}

class Activity extends ItineraryItem {
  activity_id: number = -1;
  activity_name: string = '';
  activity_url?: string;
  activity_note?: string;
  activity_time?: string;
  activity_eta?: string;

  public constructor(init?: Partial<Activity>) {
      super(init);      
      Object.assign(this, init);
      if (init && init.activity_time) {
          this.activity_time = String(init.activity_time).slice(0, 5);
      }
  }

  getInfo(): string {
      return `Activity: ${this.activity_name}`;
  }
}

class Accommodation {
  accommodation_id: number = -1;
  accommodation_name: string = '';
  check_in_date?: Date;
  check_out_date?: Date;
  address?: string;
  contact_number?: string;
  description?: string;
  accommodation_url?: string;

  public constructor(init?: Partial<Accommodation>) {
    Object.assign(this, init);
    if (init) {
      if (init.check_in_date) {
        this.check_in_date = new Date(init.check_in_date);
      }
      if (init.check_out_date) {
        this.check_out_date = new Date(init.check_out_date);
      }
    }
  }
}

class Note {
  note_id: number = -1;
  note_title?: string;
  note_content?: string;
  created_at?: Date;

  public constructor(init?: Partial<Note>) {
    Object.assign(this, init);
    if (init && init.created_at) {
      this.created_at = new Date(init.created_at);
    }
  }
}

class Expense {
  expense_id: number = -1;
  expense_name: string = '';
  amount: number = 0;
  category?: string;
  description?: string;
  expense_date?: Date;

  public constructor(init?: Partial<Expense>) {
    Object.assign(this, init);
    if (init && init.expense_date) {
      this.expense_date = new Date(init.expense_date);
    }
  }
}