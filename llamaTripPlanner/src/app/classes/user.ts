export class User {
    user_id: number = -1;
    username: string = '';
    email?: string;
    password?: string;
    created_at?: string;
    token?: string;

    public constructor(init?:Partial<User>) { 
      Object.assign(this, init);
      if (init){
        if (init.created_at){
          this.created_at = new Date(init.created_at).toISOString();
        }
      }
    }
  
    /*public constructor(user_id: number, username: string, password?: string, email: string,  created_at: string, token?: string) {
      this.user_id = user_id;
      this.username = username;
      this.email = email;
      this.password = password || '';
      this.created_at = created_at;
      this.token = token || '';
    }*/    
  }