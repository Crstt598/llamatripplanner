import { TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { DataService } from './services/dataService/data.service';
import { AuthenticationService } from './services';
import { User } from './classes/user';
import { AppComponent } from './app.component';
import { first } from 'rxjs';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let authService: AuthenticationService;
  let user: User | null;
  const testUser = {
    user_id:999,
    username:'test',
    email: 'test@llama.com',
    created_at: '2024-04-19T13:56:43.000Z',
    token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjo5OTksImlhdCI6MTcxMzU1MzQ2MywiZXhwIjoxNzE0MTU4MjYzfQ.OJi3BYAkPjCpIqNCV59Gadnf0K8ceqqYe8J820SoPRc'
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NgbModule, HttpClientModule, AppComponent],
      providers: [DataService, AuthenticationService]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    authService = TestBed.inject(AuthenticationService);
    const username = 'test';
    const password = 'test';
    authService.login(username, password)
      .pipe(first())
      .subscribe({
        next: () => {
          //console.log('login successful');
        },
        error: error => {
          console.log('error', error);
        }
      });

    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should have title initialized', () => {
    expect(component.title).toEqual('llamaTripPlanner');
  });

  it('should have correctly logged in with test user', () => {
    
    const subscriptionSpy = jasmine.createSpyObj('Subscription', ['unsubscribe']);
  
    spyOn(authService.user, 'subscribe').and.callFake(() => {
      return subscriptionSpy;
    });
    component.authenticationService.user.subscribe(x => component.user = x);    
    expect(component.user?.user_id).toEqual(testUser.user_id);
  });
});