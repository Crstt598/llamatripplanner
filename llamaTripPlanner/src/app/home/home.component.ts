import { Component } from '@angular/core';
import { PreviousTripsComponent } from './previous-trips/previous-trips.component';
import { ReportsComponent } from './reports/reports.component';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [PreviousTripsComponent, ReportsComponent],
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss'
})
export class HomeComponent {

}
