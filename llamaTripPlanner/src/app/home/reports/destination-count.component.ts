import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Destination, Trip, Day, Activity } from '@app/classes/trip';
import { DataService } from '@app/services/dataService/data.service';

@Component({
    selector: 'app-destination-count-report',
    standalone: true,
    imports: [CommonModule],
    template: `
      <h3>Destinations</h3>
      <table class="table">
        <tr>
          <th>Visited</th>
          <th>To Be Visited</th>
        </tr>
        <tr>
          <td>{{ visitedDestinationsCount }}</td>
          <td>{{ toBeVisitedDestinationsCount }}</td>
        </tr>
      </table>
    `
  })
  export class DestinationCountReportComponent implements OnInit {
    visitedDestinations: Destination[] = [];
    toBeVisitedDestinations: Destination[] = [];
  
    get visitedDestinationsCount(): number {
      return this.visitedDestinations.length;
    }
  
    get toBeVisitedDestinationsCount(): number {
      return this.toBeVisitedDestinations.length;
    }
  
    constructor(private dataService: DataService) {}
  
    ngOnInit() {
      // Fetch all trips and their destinations
      this.dataService.getAllTrips().subscribe(
        (trips) => {
          trips.forEach((trip: Trip) => {
            this.dataService.getTrip(trip.trip_id).subscribe(
              (response) => {
                trip = new Trip(response.trip);
                const today = new Date();

                trip.destinations = trip.destinations.map((destination: any) => {
                    destination = new Destination(destination);
                    destination.days = destination.days.map((day: any) => {
                        day = new Day(day);
                        day.activities = day.activities.map((activity: any) => new Activity(activity));
                        return day;
                    });
                    return destination;
                });
                trip.destinations.forEach((destination: Destination) => {
                  if (destination.departure_date && destination.departure_date.getTime() < today.getTime()) {
                    this.visitedDestinations.push(destination);
                  } else {
                    this.toBeVisitedDestinations.push(destination);
                  }
                });
              },
              (error) => {
                console.error('Error fetching trip data:', error);
              }
            );
          });
        },
        (error) => {
          console.error('Error fetching trips:', error);
        }
      );
    }
  }