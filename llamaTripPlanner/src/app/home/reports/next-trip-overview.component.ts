import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { Trip } from '@app/classes/trip';
import { DataService } from '@app/services/dataService/data.service';

@Component({
  selector: 'app-next-trip-overview-report',
  standalone: true,
  imports: [CommonModule],
  template: `
    <div *ngIf="upcomingTrips.length > 0; else noUpcomingTrip">
      <h3>Your Upcoming Trips:</h3>
      <table class="table">
        <colgroup>
          <col />
          <col />
          <col />
          <col style="width: 60%;" />
        </colgroup>
        <tr>
          <th>Start Date</th>
          <th>End Date</th>
          <th>Location</th>
          <th>Description</th>
        </tr>
        <tr *ngFor="let trip of upcomingTrips">
          <td>{{ trip.start_date | date: 'mediumDate' }}</td>
          <td>{{ trip.end_date | date: 'mediumDate' }}</td>
          <td>{{ trip.location }}</td>
          <td>{{ trip.description }}</td>
        </tr>
      </table>
    </div>
    <ng-template #noUpcomingTrip>
      <p>No upcoming trips.</p>
    </ng-template>
  `
})
export class NextTripOverviewReportComponent {
  upcomingTrips: Trip[] = [];

  constructor(private dataService: DataService) {
    this.dataService.getAllTrips().subscribe(      
      (data: Trip[]) => {
        const today = new Date();
        this.upcomingTrips = data.map((trip: any) => new Trip(trip));
        this.upcomingTrips = this.upcomingTrips.filter(
          (trip: Trip) =>
            trip.start_date && new Date(trip.start_date).getTime() >= today.getTime()
        );
        this.upcomingTrips.sort((a: Trip, b: Trip) =>
          new Date(a.start_date!).getTime() - new Date(b.start_date!).getTime()
        );
      },
      (error) => {
        console.error('Error fetching trips:', error);
      }
    );
  }
}