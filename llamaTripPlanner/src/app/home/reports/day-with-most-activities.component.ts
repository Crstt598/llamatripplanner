import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { Destination, Trip, Day, Activity } from '@app/classes/trip';
import { DataService } from '@app/services/dataService/data.service';
import { Observable, forkJoin, map } from 'rxjs';

@Component({
    selector: 'app-day-with-most-activities-report',
    standalone: true,
    imports: [CommonModule],
    template: `
      <div *ngIf="dayWithMostActivities; else noActivities">
        <h3>Day with the Most Activities</h3>
        <table class="table">
          <tr>
            <th>Date</th>
            <th>Trip</th>
            <th>Destination</th>
            <th>Activities</th>
          </tr>
          <tr>
            <td>{{ dayWithMostActivities.date | date:'mediumDate' }}</td>
            <td>{{ trip?.trip_name }}</td>
            <td>{{ destination?.destination_name }}</td>
            <td>
              <ul>
                <li *ngFor="let activity of dayWithMostActivities.activities">{{ activity.activity_name }}</li>
              </ul>
            </td>
          </tr>
        </table>
      </div>
      <ng-template #noActivities>
        <p>No activities found.</p>
      </ng-template>
    `
  })
  export class DayWithMostActivitiesReportComponent {
    dayWithMostActivities: Day | undefined;
    trip: Trip | undefined;
    destination: Destination | undefined;
    trips: Trip[] = [];
  
    constructor(private dataService: DataService) {
      this.retrieveTrips();
    }
  
    private retrieveTrips() {
        const getTripObservables: Observable<Trip>[] = [];
    
        this.dataService.getAllTrips().subscribe((response) => {
            response.forEach((trip: Trip) => {
                const getTripObservable = this.dataService.getTrip(trip.trip_id).pipe(map((response) => {
                    return new Trip(response.trip);
                }));
                getTripObservables.push(getTripObservable);
            });

            forkJoin(getTripObservables).subscribe({
                next: (retrievedTrips: Trip[]) => {
                    this.trips = retrievedTrips;
                    this.findDayWithMostActivities(retrievedTrips);
                }, error: (error: any) => {
                    console.error('An error occurred while retrieving trips:', error);
                }
            });
        }, (error) => {
            console.error('An error occurred while retrieving trip ids:', error);
        });
    }

    private findDayWithMostActivities(trips: Trip[]) {
        let dayWithMostActivities: { day: Day; destination: Destination; trip: Trip } | undefined;
        let maxActivityCount = 0;
      
        trips.forEach((trip) => {
          trip.destinations.forEach((destination) => {
            destination.days.forEach((day) => {
              if (day.activities.length > maxActivityCount) {
                maxActivityCount = day.activities.length;
                dayWithMostActivities = { day, destination, trip };
              }
            });
          });
        });
      
        if (dayWithMostActivities) {
          this.dayWithMostActivities = dayWithMostActivities.day;
          this.destination = dayWithMostActivities.destination;
          this.trip = dayWithMostActivities.trip;
        } else {
          this.dayWithMostActivities = undefined;
          this.destination = undefined;
          this.trip = undefined;
        }
      }
  }