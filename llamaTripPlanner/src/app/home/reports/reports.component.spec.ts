import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReportsComponent } from './reports.component';
import { NextTripOverviewReportComponent } from './next-trip-overview.component';
import { DestinationCountReportComponent } from './destination-count.component';
import { DayWithMostActivitiesReportComponent } from './day-with-most-activities.component';
import { CommonModule } from '@angular/common';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DataService } from '@app/services/dataService/data.service';
import { Trip } from '@app/classes/trip';
import { of } from 'rxjs';

describe('ReportsComponent', () => {
  let component: ReportsComponent;
  let fixture: ComponentFixture<ReportsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CommonModule, ReportsComponent, NextTripOverviewReportComponent, DestinationCountReportComponent, DayWithMostActivitiesReportComponent, RouterTestingModule, HttpClientTestingModule]
    }).compileComponents();

    fixture = TestBed.createComponent(ReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a defined now property', () => {
    expect(component.now).toBeTruthy();
    expect(component.now).toBeInstanceOf(Date);
  });
});

describe('NextTripOverviewReportComponent', () => {
  let component: NextTripOverviewReportComponent;
  let fixture: ComponentFixture<NextTripOverviewReportComponent>;
  let dataServiceSpy: jasmine.SpyObj<DataService>;
  const today = new Date();
  const dayPast15 = new Date(today.getTime() - (15 * 24 * 60 * 60 * 1000));
  const dayPast10 = new Date(today.getTime() - (10 * 24 * 60 * 60 * 1000));
  const dayFuture15 = new Date(today.getTime() + (15 * 24 * 60 * 60 * 1000));
  const dayFuture30 = new Date(today.getTime() + (30 * 24 * 60 * 60 * 1000));
  const dayFuture45 = new Date(today.getTime() + (45 * 24 * 60 * 60 * 1000));
  const dayFuture60 = new Date(today.getTime() + (60 * 24 * 60 * 60 * 1000));

  beforeEach(async () => {    

    const mockTrips = [
      { trip_id: 10000, user_id: 999, trip_name: 'Test Trip 1', start_date: dayPast15.toISOString(), end_date: dayPast10.toISOString(), location: 'Hawaii', description: 'A test trip for the user with ID 999' },
      { trip_id: 10001, user_id: 999, trip_name: 'Test Trip 2', start_date: dayFuture15.toISOString(), end_date: dayFuture30.toISOString(), location: 'New York', description: 'Another test trip for the user with ID 999' },
      { trip_id: 10002, user_id: 999, trip_name: 'Another Trip', start_date: dayFuture45.toISOString(), end_date: dayFuture60.toISOString(), location: 'California', description: 'Yet another test trip for the user with ID 999'}
    ];

    const spy = jasmine.createSpyObj('DataService', ['getAllTrips']);
    spy.getAllTrips.and.returnValue(of(mockTrips));

    await TestBed.configureTestingModule({
      imports: [NextTripOverviewReportComponent, DayWithMostActivitiesReportComponent, CommonModule, RouterTestingModule, HttpClientTestingModule],
      providers: [{ provide: DataService, useValue: spy }],
    }).compileComponents();

    fixture = TestBed.createComponent(NextTripOverviewReportComponent);
    component = fixture.componentInstance;
    dataServiceSpy = TestBed.inject(DataService) as jasmine.SpyObj<DataService>;
    fixture.detectChanges(); 
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should fetch upcoming trips from the data service', () => {

    const mockTrips: Trip[] = [
      new Trip({ trip_id: 10001, user_id: 999, trip_name: 'Test Trip 2', start_date: dayFuture15, end_date: dayFuture30, location: 'New York', description: 'Another test trip for the user with ID 999' }),
      new Trip({ trip_id: 10002, user_id: 999, trip_name: 'Another Trip', start_date: dayFuture45, end_date: dayFuture60, location: 'California', description: 'Yet another test trip for the user with ID 999'})
    ];

    fixture.detectChanges();

    expect(component.upcomingTrips).toEqual(mockTrips);
    expect(component.upcomingTrips.length).toBe(2);
    expect(component.upcomingTrips).toEqual(
      mockTrips.sort((a, b) => a.start_date!.getTime() - b.start_date!.getTime())
    );
  });

  it('should not show upcoming trips if there are none', () => {
    component.upcomingTrips = [];

    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('p').textContent).toContain('No upcoming trips.');

  });
});

describe('DestinationCountReportComponent', () => {
  let component: DestinationCountReportComponent;
  let fixture: ComponentFixture<DestinationCountReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DestinationCountReportComponent, CommonModule, RouterTestingModule, HttpClientTestingModule],
    }).compileComponents();

    fixture = TestBed.createComponent(DestinationCountReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});

describe('DayWithMostActivitiesReportComponent', () => {
  let component: DayWithMostActivitiesReportComponent;
  let fixture: ComponentFixture<DayWithMostActivitiesReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DayWithMostActivitiesReportComponent, CommonModule, RouterTestingModule, HttpClientTestingModule],
    }).compileComponents();

    fixture = TestBed.createComponent(DayWithMostActivitiesReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});