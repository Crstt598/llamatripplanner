import { Component } from '@angular/core';
import { NextTripOverviewReportComponent } from "./next-trip-overview.component";
import { DestinationCountReportComponent } from "./destination-count.component";
import { DayWithMostActivitiesReportComponent } from "./day-with-most-activities.component";
import { CommonModule } from '@angular/common';

@Component({
    selector: 'app-reports',
    standalone: true,
    templateUrl: './reports.component.html',
    styleUrl: './reports.component.scss',
    imports: [CommonModule, NextTripOverviewReportComponent, DestinationCountReportComponent, DayWithMostActivitiesReportComponent]
})
export class ReportsComponent {
    now: Date = new Date();
    constructor() {}
}
