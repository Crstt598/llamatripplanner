import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { Trip } from '@app/classes/trip';
import { DataService } from '@app/services/dataService/data.service';

@Component({
  selector: 'app-previous-trips',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule],
  templateUrl: './previous-trips.component.html',
  styleUrl: './previous-trips.component.scss'
})
export class PreviousTripsComponent implements OnInit{

  trips : Trip[] = [];
  searchTerm: FormControl;
  dataService: DataService;

  constructor(dataService: DataService) {
    this.dataService = dataService;
    this.searchTerm = new FormControl('');    
  }
  ngOnInit() {
    this.dataService.getAllTrips().subscribe((data : any[]) => {
      this.trips = data.map((trip: any) => new Trip(trip));
   }, (error: any) => {
      console.error('Error:', error);
    });
  }

  get filteredTrips() {
    return this.trips.filter(trip => 
      trip.trip_name.toLowerCase().includes(this.searchTerm.value.toLowerCase())
    );
  }
}
