import { ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';
import { PreviousTripsComponent } from './previous-trips.component';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { DataService } from '@app/services/dataService/data.service';
import { Trip } from '@app/classes/trip';
import { of } from 'rxjs';

describe('PreviousTripsComponent', () => {
  let component: PreviousTripsComponent;
  let fixture: ComponentFixture<PreviousTripsComponent>;
  let httpMock: HttpTestingController;
  let dataServiceSpy: jasmine.SpyObj<DataService>;

  beforeEach(async () => {
    
    const mockTrips = [
      { trip_id: 10000, user_id: 999, trip_name: 'Test Trip 1', start_date: '2024-04-01T04:00:00.000Z', end_date: '2024-04-10T04:00:00.000Z', location: 'Hawaii', description: 'A test trip for the user with ID 999' },
      { trip_id: 10001, user_id: 999, trip_name: 'Test Trip 2', start_date: '2024-07-15T04:00:00.000Z', end_date: '2024-07-25T04:00:00.000Z', location: 'New York', description: 'Another test trip for the user with ID 999' },
      { trip_id: 10002, user_id: 999, trip_name: 'Another Trip', start_date: '2024-08-01T04:00:00.000Z', end_date: '2024-08-10T04:00:00.000Z', location: 'California', description: 'Yet another test trip for the user with ID 999'}
    ];

    const spy = jasmine.createSpyObj('DataService', ['getAllTrips']);
    spy.getAllTrips.and.returnValue(of(mockTrips));
    
    await TestBed.configureTestingModule({
      imports: [PreviousTripsComponent, RouterTestingModule, HttpClientTestingModule],
      providers: [
        { provide: DataService, useValue: spy }
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PreviousTripsComponent);
    component = fixture.componentInstance;
    httpMock = TestBed.inject(HttpTestingController);
    dataServiceSpy = TestBed.inject(DataService) as jasmine.SpyObj<DataService>;
    fixture.detectChanges();    
    component.ngOnInit();
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fetch trips from DataService', fakeAsync(() => {
    
    fixture.detectChanges();    
  
    expect(component.trips.length).toBe(3);
    expect(component.trips[0] instanceof Trip).toBeTrue();
    expect(component.trips[0].trip_name).toBe('Test Trip 1');
    expect(component.trips[1].trip_name).toBe('Test Trip 2');
  }));

  it('should filter trips based on searchTerm', () => {
    component.trips = [
      new Trip({ trip_id: 10000, trip_name: 'Test Trip 1' }),
      new Trip({ trip_id: 100001, trip_name: 'Test Trip 2' }),
      new Trip({ trip_id: 100003, trip_name: 'Another Trip' })
    ];

    component.searchTerm.setValue('test trip');
    const filteredTrips = component.filteredTrips;

    expect(filteredTrips.length).toBe(2);
    expect(filteredTrips[0].trip_name).toBe('Test Trip 1');
    expect(filteredTrips[1].trip_name).toBe('Test Trip 2');
  });
});
