import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { DataService } from './services/dataService/data.service';
import { AuthenticationService } from './services';
import { User } from './classes/user';
import { PlannerComponent } from "./main/planner/planner.component";
import { LlamaApiComponent } from "./main/llama-api/llama-api.component";
import { MainComponent } from "./main/main.component";

@Component({
    selector: 'app-root',
    standalone: true,
    providers: [DataService, AuthenticationService],
    templateUrl: './app.component.html',
    styleUrl: './app.component.scss',
    imports: [RouterOutlet, NgbModule, HttpClientModule, PlannerComponent, LlamaApiComponent, MainComponent]
})
export class AppComponent{
    title = 'llamaTripPlanner';

    user?: User | null;

    constructor(public authenticationService: AuthenticationService) {
        this.user = this.authenticationService.userValue;
        //this.authenticationService.user.subscribe(x => this.user = x);        
    }
}
