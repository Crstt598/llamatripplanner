import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, map } from 'rxjs';
import { Activity, Day, Destination } from '../../classes/trip';
import { environment } from '@environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  saveTrip(data: any): Observable<any> {
    return this.http.post<any>(environment.apiUrl+'/save/trip', data);
  }

  saveDestination(trip_id: number,destination: Destination): Observable<any> {
    return this.http.post<any>(environment.apiUrl+'/save/destination', {'trip_id':trip_id,'destination':destination});
  }

  saveDay(destination_id: number,day: Day): Observable<any> {
    return this.http.post<any>(environment.apiUrl+'/save/day', {'destination_id':destination_id,'day':day});
  }
  
  saveTripWhole(data: any): Observable<any> {
    return this.http.post<any>(environment.apiUrl+'/save/trip-whole', data);
  }

  saveActivity(day_id: number, activity: Activity): Observable<any> {
    return this.http.post<any>(environment.apiUrl+'/save/activity', {'day_id':day_id,'activity':activity});
  }

  updateActivity(activity: Activity): Observable<any> {
    return this.http.post<any>(environment.apiUrl+'/update/activity', {'activity':activity});
  }

  deleteActivity(activity_id: number): Observable<any> {
    return this.http.post<any>(environment.apiUrl+'/delete/activity', {'activity_id':activity_id});
  }

  getTrip(trip_id : number): Observable<any> {
    return this.http.get<any>(environment.apiUrl+'/get/trip/?trip_id='+trip_id);
  }

  getUsersTrip_ids(): Observable<any> {
    return this.http.get<any>(environment.apiUrl+'/get/trip_ids');
  }

  getAllTrips(): Observable<any> {
    return this.http.get<any>(environment.apiUrl+'/get/all-trips');
  }
}
