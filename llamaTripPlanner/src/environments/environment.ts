/*export const environment = 
{
    production: false,
    host: 'http://127.0.0.1:4200',
    apiUrl: 'http://127.0.0.1:3000',
    llamaUrl: 'http://104.236.201.227:8081'
};*/

export const environment = 
{
    production: true,
    host: 'http://104.236.201.227:80',
    apiUrl: 'http://104.236.201.227:3000',
    llamaUrl: 'http://104.236.201.227:8080'
};