#!/bin/bash
while true
do
   ./llava-v1.5-7b-q4.llamafile -n -1 --host 127.0.0.1 --nobrowser --mlock 
   
    # Sleep for 600 seconds (10 minutes)
    sleep 600

    # Kill the llama-v1.5-7b-q4.llamafile.exe process
    pkill llava-v1.5-7b-q4.llamafile
done
