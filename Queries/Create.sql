--- Drop database
DROP DATABASE `llamatripplanner`;

-- Create database
CREATE DATABASE IF NOT EXISTS llamatripplanner;

-- Use the database
USE llamatripplanner;

-- Table for users
CREATE TABLE IF NOT EXISTS users (
    user_id INT AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(50) NOT NULL,
    email VARCHAR(100) NOT NULL,
    password VARCHAR(100) NOT NULL,
    admin bool NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- Table for trips
CREATE TABLE IF NOT EXISTS trips (
    trip_id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    trip_name VARCHAR(100) NOT NULL,
    start_date DATE,
    end_date DATE,
    location VARCHAR(255), 	
    description TEXT,
    FOREIGN KEY (user_id) REFERENCES users(user_id)
);

-- Table for destinations
CREATE TABLE IF NOT EXISTS destinations (
    destination_id INT AUTO_INCREMENT PRIMARY KEY,
    trip_id INT,
    destination_name VARCHAR(100) NOT NULL,
    arrival_date DATE,
    departure_date DATE,
    description TEXT,
    FOREIGN KEY (trip_id) REFERENCES trips(trip_id)
);

CREATE TABLE IF NOT EXISTS accommodations (
    accommodation_id INT AUTO_INCREMENT PRIMARY KEY,
    trip_id INT,
    accommodation_name VARCHAR(100) NOT NULL,
    check_in_date DATE,
    check_out_date DATE,
    address VARCHAR(255),
    contact_number VARCHAR(20),
    description TEXT,
    accommodation_url VARCHAR(255),
    FOREIGN KEY (trip_id) REFERENCES trips(trip_id)
);

-- Table for days
CREATE TABLE IF NOT EXISTS days (
    day_id INT AUTO_INCREMENT PRIMARY KEY,
    destination_id INT,
    accommodation_id INT,
    date DATE NOT NULL,
    day_number INT NOT NULL,
    description TEXT,
    FOREIGN KEY (destination_id) REFERENCES destinations(destination_id),
    FOREIGN KEY (accommodation_id) REFERENCES accommodations(accommodation_id)
);

-- Table for activities
CREATE TABLE IF NOT EXISTS activities (
    activity_id INT AUTO_INCREMENT PRIMARY KEY,
    day_id INT,
    activity_name VARCHAR(100) NOT NULL,
    activity_url VARCHAR(255),
    activity_note TEXT,
    activity_time TIME,
    activity_eta TIME,
    FOREIGN KEY (day_id) REFERENCES days(day_id)
);

-- Table for expenses
CREATE TABLE IF NOT EXISTS expenses (
    expense_id INT AUTO_INCREMENT PRIMARY KEY,
    trip_id INT,
    activity_id INT,
    expense_name VARCHAR(100) NOT NULL,
    amount DECIMAL(10, 2) NOT NULL,
    category VARCHAR(50),
    description TEXT,
    expense_date DATE,
    FOREIGN KEY (trip_id) REFERENCES trips(trip_id),
    FOREIGN KEY (activity_id) REFERENCES activities(activity_id)
);

-- Table for notes
CREATE TABLE IF NOT EXISTS notes (
    note_id INT AUTO_INCREMENT PRIMARY KEY,
    trip_id INT,
    day_id INT,
    note_title VARCHAR(100),
    note_content TEXT,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (trip_id) REFERENCES trips(trip_id),
    FOREIGN KEY (day_id) REFERENCES days(day_id)
);

INSERT INTO `users`
(`user_id`, `username`, `password`, `email`, `admin`, `created_at`)
VALUES
(1, 'user1', 'user', 'user1@llama.com', false, NOW()),
(2, 'user2', 'user', 'user2@llama.com', false, NOW()),
(999, 'test', 'test', 'test@llama.com', true, NOW());
